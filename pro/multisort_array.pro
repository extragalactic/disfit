;+
; NAME:
;   MULTISORT_ARRAY
;
; AUTHOR:
;   Craig B. Markwardt, NASA/GSFC Code 662, Greenbelt, MD 20770
;   craigm@lheamail.gsfc.nasa.gov
;   Igor V. Chilingarian, CfA
;
; PURPOSE:
;   Perform a sorting operation with multiple sort keys
;
; CALLING SEQUENCE:
;   INDICES = MULTISORT(KEY1, KEY2, ..., [/L64, ], [ORDER=order])
;
; DESCRIPTION: 
;
;   See the original MULTISORT function by Craig B. Markwardt
;
;   This is a modification of the MULTISORT function that takes 
;   input as a two-dimensional array of keys and, hence, removes 
;   the artificial limitation of 10 vectors from the original routine
;
;   The input array's dimensions are N_elements*N_keys
;   All the remaining arguments and keywords are identical to MULTISORT
;
; ========================================================
; MAIN ROUTINE
; ========================================================
function multisort_array, x_arr, L64=L64, $
                    keys=keys0, order=order0, no_builtin=nobuiltin

  COMPILE_OPT strictarr

  tmp = multisort(0) ;; a nasty trick to compile subroutines from the original CM's routine
  s_x_arr=size(x_arr)
  nkeys = (s_x_arr[0] eq 1)? 1 : s_x_arr[2]
  if nkeys EQ 0 then begin
      message, 'USAGE: INDICES = MULTISORT_ARRAY(KEY_ARRAY)', /info
      return, -1L
  endif

  order = intarr(nkeys) + 1
  if n_elements(order0) GT 0 then order[0] = round(order0)

  ;; Special case: only one term, no need to do complicate sort key
  ;; manipulations.
  if nkeys EQ 1 AND order[0] EQ +1 AND NOT keyword_set(nobuiltin) then begin
      return, sort(x_arr[*,0], L64=L64)
  endif

  ;; Master key
  mkey = ''
  for i = 0, nkeys-1 do begin
      xi = 0
      dummy = temporary(xi)
      xi = x_arr[*,i]

      sz = size(xi)
      tp = sz[sz[0]+1]
      o = order[i]
      case tp of 
          1:  mkey = temporary(mkey) + multisort_intkey(temporary(xi),2,/u,o=o)  ;; BYTE
          2:  mkey = temporary(mkey) + multisort_intkey(temporary(xi),4,o=o)     ;; INT
          3:  mkey = temporary(mkey) + multisort_intkey(temporary(xi),8,o=o)     ;; LONG
          4:  mkey = temporary(mkey) + multisort_fltkey(temporary(xi),4,o=o)     ;; FLOAT
          5:  mkey = temporary(mkey) + multisort_fltkey(temporary(xi),5,o=o)     ;; DOUBLE
          7:  mkey = temporary(mkey) + multisort_strkey(temporary(xi),o=o)       ;; STRING
          12: mkey = temporary(mkey) + multisort_intkey(temporary(xi),4,/u,o=o)  ;; UINT
          13: mkey = temporary(mkey) + multisort_intkey(temporary(xi),8,/u,o=o)  ;; ULONG
          14: mkey = temporary(mkey) + multisort_intkey(temporary(xi),16,o=o)    ;; LONG64
          15: mkey = temporary(mkey) + multisort_intkey(temporary(xi),16,/u,o=o) ;; ULONG64
          else: begin
              message, string(tp, i, $
                format='("ERROR: data type ",I0," for parameter X,",I0," is not sortable")')
              return, -1L
          end
      endcase

      xi = 0
  endfor

  return, sort(mkey, L64=L64)
end
