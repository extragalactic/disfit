; INPUTS:
;
;   ARR_VEC -- an NxM array of M N-dimensional vectors


function uniq_vectors, arr_vec, sort_idx, eps=eps
;  on_error, 2
    if(n_elements(eps) ne 1) then eps=double((machar()).epsneg)

    s_arr=size(arr_vec)
    if (s_arr[0] ne 2) then return,0

    if n_params() eq 1 then begin
        idx = where(total((arr_vec-shift(arr_vec,0,-1))^2,1) gt eps^2, c_idx)
        return, (c_idx gt 0)? idx : -1L
    endif else begin
        tmp = arr_vec[*,sort_idx]
        idx = where(total((tmp-shift(tmp,0,-1))^2,1) gt eps^2, c_idx)
        return, (c_idx gt 0)? sort_idx[idx] : -1L
    endelse
end
