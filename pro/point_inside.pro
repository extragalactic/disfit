;------------------------------------------------------------------------------
;+
; FILENAME: points_inside.pro
;
; VERSION: 1.5.210125
;-
;______________________________________________________________________________

;------------------------------------------------------------------------------
;---------------------------   POINT_INSIDE__NORMAL   -------------------------
;------------------------------------------------------------------------------

function point_inside__normal, xins, xpnt, normal=normal, noboundary=noboundary, $
    zero=zero, keyboundary=key_boundary, debug=debug

compile_opt hidden

key_boundary = 0

if (keyword_set(debug)) then print, normal ## transpose([xpnt,1d]), normal ## transpose([xins,1d]), zero, format='("DISTANCE (ins,pnt,zero):",tr3,f,tr3,f,tr3,"(",f,")")'

sign_xpnt = normal ## transpose([xpnt,1d])
if (abs(sign_xpnt) lt zero) then sign_xpnt = 0d
if (sign_xpnt ne 0d) then sign_xpnt /= abs(sign_xpnt) else begin
        key_boundary = 1
        return, (key_pnt = (keyword_set(noboundary))? 0 : 1)
    endelse

sign_xins = normal ## transpose([xins,1d])
if (abs(sign_xins) lt zero) then sign_xins = 0d
if (sign_xins ne 0d) then sign_xins /= abs(sign_xins) else return, (key_pnt = 0)

return, (key_pnt = (sign_xpnt eq sign_xins)? 1 : 0)
end
;______________________________________________________________________________

;==============================================================================
;=============================   POINT_INSIDE_SIMPLEX   =======================
;==============================================================================

function point_inside, xarr, xpnt, normals=normals, noboundary=noboundary, nozeronormals=nozeronormals, $
    double=double, zero=zero, boundary=key_boundary, debug=debug, _extra=extra

key_pnt = 1
key_boundary = 0
if (n_elements(nozeronormals) ne 1) then nozeronormals = 1

s_xarr = size(xarr)
d_xarr = (s_xarr[0] ge 1l)? s_xarr[1] : (s_xarr[s_xarr[0]+2] eq 1)? 1l : 0l
n_xarr = (d_xarr gt 0l)? (s_xarr[0] eq 2)? s_xarr[2] : (s_xarr[0] eq 1)? 1l : 0l : 0l

s_xpnt = size(xpnt)
d_xpnt = (s_xpnt[0] ge 1l)? s_xpnt[1] : (s_xpnt[s_xpnt[0]+2] eq 1)? 1l : 0l
n_xpnt = (d_xpnt gt 0l)? (s_xpnt[0] eq 2)? s_xpnt[2] : (s_xpnt[0] eq 1)? 1l : 0l : 0l

s_normals = size(normals)
d_normals = (s_normals[0] gt 0l)? s_normals[1] : 0l
n_normals = (s_normals[0] eq 2l)? s_normals[2] : (s_normals[0] eq 1l)? 1l : 0l

s_zero = size(zero)
n_zero = (s_zero[0] eq 0)? (s_zero[s_zero[0]+2] eq 1l)? 1l : 0l : 0l
normal_zero = (n_zero eq 1l)? zero : (keyword_set(double))? 1d-12 : 1d-6

if ~((d_xarr gt 0l) and (d_xarr eq d_xpnt)) then begin
    message, /info, 'incorrect (xarr/xinside)/xpnt'
    return, (key_pnt = 0)
endif

if ((d_xarr gt 0l) and (n_xarr eq 1l) and (n_xpnt eq 1l) and (d_normals eq d_xarr+1l) and (n_normals gt 0l)) then begin
    for i_normals=0l, n_normals-1l do $
        if (keyword_set(nozeronormals) and (total(normals[*,i_normals]*normals[*,i_normals], /double) gt 0d)) then $
            if (point_inside__normal(xarr, xpnt, normal=normals[*,i_normals], $
                    noboundary=noboundary, zero=normal_zero, keyboundary=key_boundary, debug=debug) ne 1) then return, (key_pnt = 0)
endif else if ((d_xarr gt 0l) and (n_xarr eq d_xarr+1l) and (n_normals eq 0l)) then begin
    if (la_determ([xarr,dblarr(1,n_xarr)+1d], /check, /double) eq 0d) then return, (key_pnt = 0)
    ind_gen = lindgen(n_xarr)
    for i_xarr=0l, n_xarr-1l do $
        if (keyword_set(nozeronormals) and (total(((normal = normvec(xarr[*,where(ind_gen ne i_xarr)], debug=debug, _extra=extra)))^2d, /double) gt 0d)) then $
            if (point_inside__normal(xarr[*,i_xarr], xpnt, normal=normal, noboundary=noboundary, zero=normal_zero, keyboundary=key_boundary, debug=debug) ne 1) then return, (key_pnt = 0)
endif else begin
    message, /info, 'incorrect input'
    return, (key_pnt = 0)
endelse

return, key_pnt
end
;______________________________________________________________________________
