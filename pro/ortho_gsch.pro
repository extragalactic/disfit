;------------------------------------------------------------------------------
;+
; FUNCTION: ortho_gsch.pro
;
; DESCRIPTION: the Gram-Schmidt process
;
; VERSION: 1.2.210415
;-
;______________________________________________________________________________

;------------------------------------------------------------------------------
;----------------------------   ORTHO_GSCH__INFO   ----------------------------
;------------------------------------------------------------------------------

function ortho_gsch__info, inp, s=s_inp, d=d_inp, t=t_inp, n=n_inp, p=p_inp, q=q_inp, k=k_inp

compile_opt strictarr, hidden

key = 0
d_inp = (t_inp = (n_inp = (p_inp = (q_inp = (k_inp = 0l)))))

if (key = (n_params() eq 1l)) then s_inp = size(inp) $
    else if (key = ((n_s_inp = n_elements(s_inp)) gt 0l)) then $
        if (key = (n_s_inp eq s_inp[0]+3l)) then key = (product(s_inp[1:s_inp[0]]) eq s_inp[s_inp[0]+2l])

if ~(keyword_set(key)) then begin
    message, /info, "undefined/incorrect INPUT/SIZE(INPUT)"   
    return, [0l,0l,0l]
endif

d_inp = s_inp[0]
t_inp = s_inp[s_inp[0]+1l]
n_inp = s_inp[s_inp[0]+2l]
p_inp = (s_inp[0] gt 0l)? s_inp[1] : (n_inp eq 1l)? 1l : 0l
q_inp = (s_inp[0] gt 1l)? s_inp[2] : ((p_inp gt 0l) and (n_inp eq p_inp))? 1l : 0l
k_inp = (~array_equal(s_inp,[0l,0l,0l]) and (s_inp[0] le 2))

return, s_inp
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;---------------------------   ORTHO_GSCH__SCALAR   ---------------------------
;------------------------------------------------------------------------------

function ortho_gsch__scalar, a, b

compile_opt hidden

s = !values.f_nan
s_a = ortho_gsch__info(a, t=t_a, n=n_a, p=p_a, q=q_a)
s_b = ortho_gsch__info(b, t=t_b, n=n_b, p=p_b, q=q_b)

if ((n_a gt 0l) and (((t_a eq t_b) and (p_a eq p_b) and ((q_a eq q_b) or (q_b eq 1l))) or (n_b eq 0l))) then begin
    for i_a=0l, q_a-1l do begin
        aa = a[*,i_a]
        bb = (q_b eq q_a)? b[*,i_a] : (q_b eq 1l)? b : aa
        if ((t_a eq 6l) or (t_a eq 9l)) then begin
            ss = (p_a gt 1l)? $
                aa ## transpose(((t_a eq 9l)? dcomplex(real_part(bb),-imaginary(bb)) : complex(real_part(bb),-imaginary(bb)))) : $
                aa * ((t_a eq 9l)? dcomplex(real_part(bb),-imaginary(bb)) : complex(real_part(bb),-imaginary(bb)))
        endif else ss = (p_a gt 1l)? aa ## transpose(bb) : aa * bb
        if (i_a eq 0l) then s = (q_a gt 1l)? replicate(ss*s,1l,q_a) : temporary(ss[0])
        if (q_a gt 1l) then s[0l,i_a] = temporary(ss)
    endfor
endif else message, /info, "undefined/incorrect input A or A/B"

return, s
end
;______________________________________________________________________________

;==============================================================================
;===============================   ORTHO_GSCH   ===============================
;==============================================================================

function ortho_gsch, array, maximization=maximization, normalization=normalization, $
    double=double, zero=zero, idx_basis=idx_basis, n_idx_basis=n_idx_basis

s_array = ortho_gsch__info(array, t=t_array, n=n_array, p=p_array, q=q_array)

if (n_elements(double) ne 1l) then double = 1
basis_zero = (n_elements(zero) eq 1l)? zero : p_array * ((keyword_set(double))? 1d-12 : 1d-6)
basis = (keyword_set(double))? [!values.d_nan] : [!values.f_nan]
n_idx_basis = 0l
idx_basis = -1l

;(-);if ((p_array gt 0l) and (q_array ge p_array) and (n_array eq p_array*q_array)) then begin
if ((p_array gt 0l) and (q_array gt 0l)) then begin
    grid_basis = intarr(q_array)
    grid_basis[0] = 1
    basis = array
    idx_basis = lonarr((q_basis = p_array)) - 1l
    idx_basis[0] = 0l
    n_idx_basis = 1l
    if (keyword_set(normalization)) then basis[*,idx_basis[0]] /= sqrt(ortho_gsch__scalar(basis[*,idx_basis[0]])) else basis_zero *= sqrt(ortho_gsch__scalar(basis[*,idx_basis[0]]))

    for i_basis=0l, q_basis-1l do begin
        idx_select = where((grid_basis eq 0l) and (sqrt(ortho_gsch__scalar(basis)) gt basis_zero), n_idx_select)
        if (n_idx_select gt 0l) then begin
            basis[*,idx_select] -= $
                (ortho_gsch__scalar(basis[*,idx_select],basis[*,idx_basis[i_basis]]) ## basis[*,idx_basis[i_basis]] / $
                ((keyword_set(normalization))? 1. : ortho_gsch__scalar(basis[*,idx_basis[i_basis]])))
            if (total(grid_basis,/int) eq q_basis) then break
            if (keyword_set(maximization)) then begin
                max = max(ortho_gsch__scalar(basis[*,idx_select]), i_select)
                if (max le basis_zero) then i_select = -1l
            endif else begin
                for i_select=0l, n_idx_select-1l do if (ortho_gsch__scalar(basis[*,idx_select[i_select]]) gt basis_zero) then break
                if (i_select eq n_idx_select) then i_select = -1l
            endelse
            if (i_select ne -1l) then begin
                if (keyword_set(normalization)) then basis[*,idx_select[i_select]] /= sqrt(ortho_gsch__scalar(basis[*,idx_select[i_select]])) ;(!); sqrt(ortho_gsch__scalar(basis[*,idx_basis[i_basis]]))
                idx_basis[i_basis+1l] = idx_select[i_select]
                grid_basis[idx_select[i_select]] = 1
            endif else break
        endif
    endfor
    tmp = temporary(basis)
    basis = tmp[*,(idx_basis = idx_basis[where(idx_basis ne -1l, n_idx_basis)])]
    if (n_idx_basis lt p_array) then message, /info, "incomplete basis ("+strtrim(n_idx_basis,2)+"/"+strtrim(p_array,2)+")"
;(-);basis = tmp[*,0l:(q_basis<q_array)-1l] * !values.f_nan
;(-);basis[*,0l:total(grid_basis,/int)-1l] = tmp[*,(idx_basis = idx_basis[where(idx_basis ne -1l, n_idx_basis)])]
endif else message, /info, "incorrect/undefined input ARRAY"

return, basis
end
;______________________________________________________________________________
