;------------------------------------------------------------------------------
;+
; FILENAME: mdqf.pro
;
; PURPOSE: multidimensional quadratic function
;
; CALLING SEQUENCE:
;
;   result = mdqf(action=action, $
;       xarr=xarr, yarr=yarr, warr=warr, farr=farr, carr=carr, parr=parr, $
;       idx=idx, xpos=xpos, noposdef=noposdef, nosemidef=nosemidef, $
;       coefficients=coef, weights=wght, hessian=hessian, linear=linear, sylvester=sylvester, $
;       xmin=xmin, exmin=exmin, ymin=ymin, eymin=eymin)
;
; FUNCTIONS and PROCEDURES:
;
;   mdqf__info()
;   mdqf__array()
;   mdqf__matrix()
;   mdqf__derive()
;   mdqf__hessian()
;   mdqf__sylvester()
;   mdqf__transform()
;   mdqf__minimum()
;   mdqf__weights()
;   mdqf__values()
;   mdqf__mpfit__parameters()
;   mdqf__mpfit__choldc()
;   mdqf__mpfit__fitfunc()
;   mdqf__mpfit()
;   mdqf__coefficients()
;   mdqf()
;
; EXAMPLES: (for IDL8+)
;
;   (action = 'matrix'):
;       1)  IDL> m = mdqf(act='mat', xarr=(x=randomn(!null, 2)))
;       2)  IDL> m = mdqf(act='mat', xarr=(x=randomn(!null, 3, 10)))
;       3)  IDL> m = mdqf(act='mat', xarr=(x=randomn(!null, 2)), farr=[1,0,1,0,0,1])
;       4)  IDL> m = mdqf(act='mat', xarr=(x=randomn(!null, 3, 10)), farr=[1,0,0,1,0,1,0,0,0,1])
;
;   (action = 'derive'):
;       1)  IDL> d = mdqf(act='der', xarr=(x=randomn(!null, 2)))
;       2)  IDL> d = mdqf(act='der', xarr=(x=randomn(!null, 3, 10)))
;       3)  IDL> d = mdqf(act='der', xarr=(x=randomn(!null, 2)), farr=[1,0,1,0,0,1])
;       4)  IDL> d = mdqf(act='der', xarr=(x=randomn(!null, 3, 10)), farr=[1,0,0,1,0,1,0,0,0,1])
;
;   (action = 'hessian'):
;       1)  IDL> h = mdqf(act='hes', carr=(c=randomn(!null, 10)), l=l)
;       2)  IDL> h = mdqf(act='hes', farr=(f=[1,0,0,1,0,1,0,0,0,1]), carr=(c=randomn(!null, total(f))), l=l)
;
;   (action = 'sylvester'):
;       1)  IDL> s = mdqf(act='syl', carr=(c=randomn(!null, 10)))
;       2)  IDL> s = mdqf(act='syl', carr=(c=randomn(!null, 10)), /semidef, hes=h, lin=l)
;       3)  IDL> s = mdqf(act='syl', farr=(f=[1,1,1,0,0,0]), carr=(c=randomn(!null, total(f))), hes=h, lin=l)
;
;   (action = 'minimum'):
;       1)  IDL> x = mdqf(act='min', carr=(c=randomn(!null, 10)), exmin=ex, ymin=y, eymin=ey, syl=s) ;(c); check S eq 1 else output NaN
;
;   (action = 'weights'):
;       1)  IDL> w = mdqf(act='weights', xarr=(x = randomu(!null, 3, 10)-0.5), xpos=(p = total(x,2,/double)/10))
;
;   (action = 'values'):
;       1)  IDL> v = mdqf(act='values', )
;
;   (action = 'choldc'):
;       1)  IDL> c = mdqf(act='choldc', )
;
;   (action = ['coefficients','',!null] => action = !null):
;       1)  IDL> c = mdqf(xarr=(x=randomn(!null, 3, 100)), yarr=(y=(cc=randomn(!null,10))##transpose(x)))
;
; NOTE: ask ER
;
; VERSION: 2.0.210420
;-
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-------------------------------   MDQF__INFO   -------------------------------
;------------------------------------------------------------------------------

function mdqf__info, inp, s=s_inp, d=d_inp, t=t_inp, n=n_inp, p=p_inp, q=q_inp, k=k_inp

compile_opt strictarr, hidden

key = 0
d_inp = (t_inp = (n_inp = (p_inp = (q_inp = (k_inp = 0l)))))

if (key = (n_params() eq 1l)) then s_inp = size(inp) $
    else if (key = ((n_s_inp = n_elements(s_inp)) gt 0l)) then $
        if (key = (n_s_inp eq s_inp[0]+3l)) then key = (product(s_inp[1:s_inp[0]]) eq s_inp[s_inp[0]+2l])

if ~(keyword_set(key)) then begin
    message, /info, "undefined/incorrect INPUT/SIZE(INPUT)"   
    return, [0l,0l,0l]
endif

d_inp = s_inp[0]
t_inp = s_inp[s_inp[0]+1l]
n_inp = s_inp[s_inp[0]+2l]
p_inp = (s_inp[0] gt 0l)? s_inp[1] : (n_inp eq 1l)? 1l : 0l
q_inp = (s_inp[0] gt 1l)? s_inp[2] : ((p_inp gt 0l) and (n_inp eq p_inp))? 1l : 0l
k_inp = (~array_equal(s_inp,[0l,0l,0l]) and (s_inp[0] le 2))

return, s_inp
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-------------------------------   MDQF__ARRAY   ------------------------------
;------------------------------------------------------------------------------

function mdqf__array, arr, quadratic=quadratic, upper=upper, lower=lower

;(c); function for transform square/linear matrix to linear/square matrix

compile_opt hidden

array = [!values.d_nan]
s_arr = mdqf__info(arr, n=n_arr, p=p_arr, q=q_arr)
if (n_arr eq 0l) then begin
    message, /info, "undefined input ARR"
    return, array
endif

if (p_arr gt 1l) then begin
    if (p_arr eq q_arr) then begin
;(c); square matrix to linear transformation
        if (keyword_set(quadratic)) then begin
            array = reform(arr, (p_array = p_arr*q_arr))
        endif else if (keyword_set(upper) xor keyword_set(lower)) then begin
            array = dblarr((p_array = (p_arr+1l)*q_arr/2l))
            for i=(i_array=0l), p_arr-1l do begin
                j = (keyword_set(lower))? i : (p_arr-1l-i)
                array[i_array:i_array+j] = arr[((keyword_set(lower))? 0l : i) : ((keyword_set(lower))? i : (p_arr-1l)), i]
                i_array += (temporary(j) + 1l)
            endfor
        endif else message, /info, "undefined/incorrect ARR type (QUADRATIC/UPPER/LOWER)"
    endif else if (q_arr eq 1l) then begin
;(c); linear matrix to square transformation
        if (keyword_set(quadratic)) then begin
            p_array = sqrt(d_arr)
            if (p_array eq long(p_array)) then begin
                array = reform(arr, [p_array,p_array])
            endif else message, /info, "incorrect ARR size/format"
        endif else if (keyword_set(upper) xor keyword_set(lower)) then begin
            p_array = (-1d0+sqrt(1d0+8d0*p_arr))/2d0
            if (p_array eq long(p_array)) then begin
                array = dblarr(p_array,p_array)
                for i=(i_arr=0l), p_array-1l do begin
                    j = (keyword_set(lower))? i : (p_array-1l-i)
                    array[((keyword_set(lower))? 0l : i) : ((keyword_set(lower))? i : (p_array-1l)),i] = arr[i_arr:i_arr+j]
                    i_arr += (temporary(j) + 1l)
                endfor
            endif else message, /info, "incorrect ARR size/format"
        endif else message, /info, "undefined/incorrect ARR type (QUADRATIC/UPPER/LOWER)"
    endif else message, /info, "incorrect ARR size/format"
endif else if (n_arr eq 1l) then array = [arr]

return, array
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-------------------------------   MDQF__MATRIX   -----------------------------
;------------------------------------------------------------------------------

function mdqf__matrix, xarr, farr, status=status, errmsg=errmsg

compile_opt hidden

status = 0
errmsg = ''
matrix = [!values.d_nan]
s_xarr = mdqf__info(xarr, n=n_xarr, p=p_xarr, q=q_xarr)
if (n_xarr eq 0l) then begin
    message, /info, "undefined input XARR"
    return, matrix
endif
s_farr = mdqf__info(farr, n=n_farr, p=p_farr, q=q_farr)
if (n_farr ne 0l) then begin
    if ~((p_farr eq (p_xarr+2l)*(p_xarr+1l)/2l) and (q_farr eq 1l)) then begin
        message, /info, "incorrect input FARR size/format"
        return, matrix
    endif
endif

flag = (n_farr gt 0l)? farr : bytarr((p_xarr+2l)*(p_xarr+1l)/2l) + 1b
p_matrix = total(flag,/int)
q_matrix = q_xarr

if ((p_matrix gt 0l) and (q_matrix gt 0l)) then begin
    matrix = dblarr(p_matrix,q_matrix) + !values.d_nan
    for i=(m=(f=0l)), p_xarr-1l do for j=i, p_xarr-1l do if (flag[f++] eq 1) then matrix[m++,*] = xarr[i,*] * xarr[j,*]
    for i=0l, p_xarr-1l do if (flag[f++] eq 1) then matrix[m++,*] = xarr[i,*]
    if (flag[f++] eq 1) then matrix[m,*] = 1d
endif

return, matrix
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-------------------------------   MDQF__DERIVE   -----------------------------
;------------------------------------------------------------------------------

function mdqf__derive, xarr, farr, status=status, errmsg=errmsg

compile_opt hidden

status = 0
errmsg = ''
derive = [!values.d_nan]
s_xarr = mdqf__info(xarr, n=n_xarr, p=p_xarr, q=q_xarr)
if (n_xarr eq 0l) then begin
    message, /info, "undefined input XARR"
    return, derive
endif
s_farr = mdqf__info(farr, n=n_farr, p=p_farr, q=q_farr)
if (n_farr ne 0l) then begin
    if ~((p_farr eq (p_xarr+2l)*(p_xarr+1l)/2l) and (q_farr eq 1l)) then begin
        message, /info, "incorrect input FARR size/format"
        return, derive
    endif
endif

flag = (n_farr gt 0l)? farr : bytarr((p_xarr+2l)*(p_xarr+1l)/2l) + 1b
p_derive = total(flag,/int)
q_derive = q_xarr

if ((p_derive gt 0l) and (q_derive gt 0l)) then begin
    derive = dblarr(p_derive,p_xarr,q_xarr)
    for d=0l, p_xarr-1l do begin
        for i=(m=(f=0l)), p_xarr-1l do for j=i, p_xarr-1l do $
            if (flag[f++] eq 1) then begin
                derive[m++,d,*] = $
                    ((i eq d) and (j eq d))? 2d*xarr[i,*] : $
                    ((i eq d) and (j ne d))? xarr[j,*] : $
                    ((i ne d) and (j eq d))? xarr[i,*] : 0d
            endif
        for i=0l, p_xarr-1l do if (flag[f++] eq 1) then derive[m++,d,*] = (i eq d)? 1d : 0d
    endfor
endif

return, derive
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-------------------------------   MDQF__HESSIAN   ----------------------------
;------------------------------------------------------------------------------

function mdqf__hessian, coef, linear=linear, status=status, errmsg=errmsg

compile_opt hidden

status = 0
errmsg = ''
hessian = (linear = [!values.d_nan])
s_coef = mdqf__info(coef, n=n_coef, p=p_coef, q=q_coef)
if ~((p_coef gt 0l) and (q_coef eq 1l)) then begin
    message, /info, "undefinedi/incorrect input COEF"
    return, hessian
endif

p_xarr = (-3d0+sqrt(9d0-8d0*(1d0-p_coef)))/2d
if (p_xarr eq long(p_xarr)) then begin
    linear = transpose(-coef[(p_xarr+1l)*p_xarr/2l:(p_xarr+2l)*(p_xarr+1l)/2l-2l])
    hessian = mdqf__array(coef[0l:(p_xarr+1l)*p_xarr/2l-1l], /upper)
    hessian += transpose(hessian)
endif else message, /info, "incorrect size COEF"

return, hessian
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;------------------------------   MDQF__SYLVESTER   ---------------------------
;------------------------------------------------------------------------------

function mdqf__sylvester, hessian, semidef=semidef, status=status, errmsg=errmsg

;(c); key eq  1 for positive-(semi-)definite matrix
;(c); key eq -1 for negative-(semi-)definite matrix
;(c); key eq  0 for nondefinite matrix

compile_opt hidden

status = 0
errmsg = ''
key = 0
s_hessian = mdqf__info(hessian, n=n_hessian, p=p_hessian, q=q_hessian)

if ((n_hessian gt 0l) and (p_hessian eq q_hessian)) then begin
    positive = (keyword_set(semidef))? (hessian[0l,0l] ge 0d) : (hessian[0l,0l] gt 0d)
    negative = (keyword_set(semidef))? (hessian[0l,0l] le 0d) : (hessian[0l,0l] lt 0d)
    for i=1l, p_hessian-1l do begin
        if (keyword_set(positive) or keyword_set(negative)) then begin
            det = la_determ(hessian[0l:i,0l:i], /check, /double)
            if (keyword_set(positive)) then positive *= (keyword_set(semidef))? (det ge 0d) : (det gt 0d)
            if (keyword_set(negative)) then negative *= (keyword_set(semidef))? (det*(-1d0)^(i+1l) ge 0d) : (det*(-1d0)^(i+1l) gt 0d)
        endif else break
    endfor
    key = (keyword_set(positive) xor keyword_set(negative))? (keyword_set(positive))? 1 : -1 : 0
endif

return, key
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;------------------------------   MDQF__TRANSFORM   ---------------------------
;------------------------------------------------------------------------------

function mdqf__transform, xarr=xarr, coef=coef, transform=tr, inverse=inverse

compile_opt hidden

tarr = [!values.f_nan]
s_xarr = mdqf__info(xarr, n=n_xarr, p=p_xarr, q=q_xarr)
s_coef = mdqf__info(coef, n=n_coef, p=p_coef, q=q_coef)
if (n_coef gt 0l) then begin
    p_xarr = (-3d0+sqrt(9d0-8d0*(1d0-p_coef)))/2d
    if ((p_xarr gt 0l) and (p_xarr eq long(p_xarr))) then begin
        tarr = coef + !values.f_nan
    endif else begin
        message, /info, "incorrect COEF size"
        return, tarr
    endelse
endif

s_tr = mdqf__info(tr, n=n_tr, p=p_tr, q=q_tr)
if (n_tr gt 0l) then begin
    if ~((p_tr eq p_xarr) and (q_tr eq p_xarr)) then begin
        message, /info, "incorrect input TRANSFORM"
        return, tarr
    endif
endif else begin
    if (keyword_set(inverse)) then begin
        message, /info, "unavailable action (INVERSE) for undefined TRANSFORM"
        return, tarr
    endif
    tmp = ortho_transform(mdqf__hessian(coef), tr=tr)
endelse

if ((n_xarr gt 0l) xor (n_coef gt 0l)) then begin
    if (n_xarr gt 0l) then tarr = transpose(((keyword_set(inverse))? transpose(tr) : tr) ## transpose(xarr))
    if (n_coef gt 0l) then begin
        tarr[0l:(p_xarr+1l)*p_xarr/2l-1l] = mdqf__array(((keyword_set(inverse))? (transpose(tr) ## mdqf__hessian(coef) ## tr) : (tr ## mdqf__hessian(coef) ## transpose(tr))) / (identity(p_xarr)+1), /upper)
        tarr[(p_xarr+1l)*p_xarr/2l:(p_xarr+3l)*p_xarr/2l-1l] = coef[(p_xarr+1l)*p_xarr/2l:(p_xarr+3l)*p_xarr/2l-1l] ## ((keyword_set(inverse))? tr : transpose(tr))
        tarr[(p_xarr+3l)*p_xarr/2l] = coef[(p_xarr+3l)*p_xarr/2l]
    endif
endif else begin
    message, /info, "incorrect/undefined input XARR/COEF/[TRANSFORM]"
    return, tarr
endelse

return, tarr
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;--------------------------------   MDQF__MINIMUM   ---------------------------
;------------------------------------------------------------------------------

function mdqf__minimum, coef, xdeg=xdeg, xpos=xpos, $
    xmin=xmin, exmin=exmin, ymin=ymin, eymin=eymin, hessian=hessian, linear=linear, sylvester=sylvester, $
    status=status, errmsg=errmsg

compile_opt hidden

status = 0
errmsg = ''
xmin = (exmin = (ymin = (eymin = (hessian = (linear = [(sylvester = !values.d_nan)])))))
s_coef = mdqf__info(coef, n=n_coef, p=p_coef, q=q_coef)
if (n_coef eq 0l) then begin
    message, /info, "undefined input COEF"
    return, xmin
endif else begin
    p_xarr = (-3d0+sqrt(9d0-8d0*(1d0-p_coef)))/2d
    if ((p_xarr gt 0l) and (p_xarr eq long(p_xarr))) then begin
        xmin = (exmin = dblarr(p_xarr) + !values.d_nan)
    endif else begin
        message, /info, "incorrect COEF size"
        return, xmin
    endelse
endelse

s_xdeg = mdqf__info(xdeg, n=n_xdeg, p=p_xdeg, q=q_xdeg)
if (n_xdeg ne 0l) then begin
    if ~((p_xdeg eq p_xarr) and (q_xdeg eq 1l)) then begin
        message, /info, "incorrect XDEG (degeneracy)"
        return, xmin
    endif
endif

s_xpos = mdqf__info(xpos, n=n_xpos, p=p_xpos, q=q_xpos)
if (n_xpos ne 0l) then begin
    if ~((p_xpos eq p_xarr) and (q_xpos eq 1l)) then begin
        message, /info, "incorrect XPOS"
        return, xmin
    endif
endif

if (p_xpos eq 0) then begin
    hessian_tr = ortho_transform((hessian = mdqf__hessian(coef, linear=linear)), transform=tr)
    idx_hessian_tr = where(abs(diag_matrix(hessian_tr)) gt max(abs(diag_matrix(hessian_tr))) * 1d-3, n_idx_hessian_tr, comp=idx_xdeg_tr, ncomp=n_idx_xdeg_tr)
    if (n_idx_hessian_tr gt 0l) then begin
        xmin_tr = xmin
        xmin_tr[idx_hessian_tr] = (transpose(tr ## transpose(linear)) / diag_matrix(hessian_tr))[idx_hessian_tr]
        if (n_idx_xdeg_tr gt 0l) then begin
            if (p_xdeg eq p_xarr) then begin
                xmin_tr[idx_xdeg_tr] = (transpose(tr ## transpose(xdeg)))[idx_xdeg_tr]
            endif else message, /info, "COEF is degeneracy without XDEG then NaN XMIN"
        endif
        xmin[*] = transpose(transpose(tr) ## transpose(xmin_tr))
    endif else begin
        message, /info, "incorrect XMIN"
        return, xmin
    endelse
endif else if (p_xpos eq p_xarr) then begin
    xmin = xpos
endif else begin
    message, /info, "incorrect XMIN"
    return, xmin
endelse

if (total(finite(xmin),/int) eq p_xarr) then begin
    a = diag_matrix(hessian) / 2d
    b = coef ## transpose(mdqf__derive(xmin))
    d = sqrt(b*b + a*4d)
    exmin = ((d-b)/(a*2d)) > 0d
    ymin[*] = coef ## transpose(mdqf__matrix(xmin))
    eymin[*] = sqrt((b*b) ## transpose(exmin*exmin)) > 0d
endif else message, /info, "undefined XMIN"

return, xmin
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-----------------------------   MDQF__WEIGHTS   ------------------------------
;------------------------------------------------------------------------------

function mdqf__weights, matrix, position, idx=idx, status=status, errmsg=errmsg

compile_opt hidden

status = 0
errmsg = ''
weights = [!values.d_nan]

s_matrix = mdqf__info(matrix, n=n_matrix, p=p_matrix, q=q_matrix)
if (n_matrix gt 0l) then begin
    weights = dblarr(q_matrix) + !values.d_nan
    p_xarr = (-3d0+sqrt(9d0-8d0*(1d0-p_matrix)))/2d
    if ~((p_xarr gt 0l) and (p_xarr eq long(p_xarr))) then begin
        message, /info, "incorrect size MATRIX (P_MATRIX)"
        return, weights
    endif
    if (p_matrix gt q_matrix) then begin
        message, /info, "incorrect size MATRIX (Q_MATRIX)"
        return, weights
    endif
    if ~(product(finite(matrix),/int)) then begin
        message, /info, "infinite MATRIX"
        return, weights
    endif
endif else begin
    message, /info, "undefined MATRIX"
    return, weights
endelse

s_position = mdqf__info(position, n=n_position, p=p_position, q=q_position)
if (n_position gt 0l) then begin
    if ~((p_position eq p_matrix) and (q_position eq 1l)) then begin
        message, /info, "incorrect size POSITION"
        return, weights
    endif
    if ~(product(finite(position),/int)) then begin
        message, /info, "infinite POSITION"
        return, weights
    endif
endif else begin
    message, /info, "undefined POSITION"
    return, weights
endelse

if ((n_idx = n_elements(idx)) gt 0l) then begin
    if (q_matrix gt n_idx) then begin
        tmp_flag_idx = intarr(q_matrix) + 1
        tmp_flag_idx[idx] = 0
        idx_matrix = [idx, where(temporary(tmp_flag_idx))]
    endif else idx_matrix = idx
endif else idx_matrix = lindgen(q_matrix)

;(c); select basis
;(!); print, "!!! INCORRECT BASIS DETECTION !!!" ;(!);;(stop);
if (q_matrix gt p_matrix) then begin
    tmp_dist_sqr = dblarr(q_matrix)
    if (n_idx lt q_matrix) then for i_matrix=0l, p_matrix-1l do tmp_dist_sqr[idx_matrix[n_idx:*]] += (matrix[i_matrix,idx_matrix[n_idx:*]] - position[i_matrix])^2d
    idx_matrix = idx_matrix[sort(temporary(tmp_dist_sqr))]
    idx_matrix = lindgen(q_matrix)
    basis = ortho_gsch(matrix[*,idx_matrix], /double, /normalization, idx=idx_basis, n_idx=n_idx_basis)
    if (n_idx_basis gt 0l) then begin
        idx_matrix = idx_matrix[idx_basis]
    endif else message, /info, "basis not defined"
endif

det = la_determ(matrix[*,idx_matrix], /check, /double)
if ((finite(det)) and (det ne 0d)) then begin
    weights[*] = 0d0
    det_arr = weights[idx_matrix]
    for i_det_arr=0l, n_elements(det_arr)-1l do begin
        tmp = matrix[*,idx_matrix]
        tmp[*,i_det_arr] = position
        det_arr[i_det_arr] = la_determ(temporary(tmp), /check, /double)
        if ~(finite(det_arr[i_det_arr])) then break
    endfor
    if (product(finite(det_arr),/int)) then begin
        weights[idx_matrix] = det_arr / det
        if (abs(total(weights,/double)-1d0) gt 1d-10) then message, /info, "ATTENTION! total weights ne 1 => EXTRAPOLATION"
    endif else message, /info, "det(A_i) = NaN"
endif else message, /info, "det(A) = 0 or NaN"

return, weights
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;------------------------------   MDQF__VALUES   ------------------------------
;------------------------------------------------------------------------------

function mdqf__values, x, xarr=xarr, coefficients=coef, yarr=yarr, weights=wght, status=status, errmsg=errmsg

compile_opt hidden

status = 0
errmsg = ''
varr = [!values.d_nan]

s_x = mdqf__info(x, n=n_x, p=p_x, q=q_x)
s_xarr = mdqf__info(xarr, n=n_xarr, p=p_xarr, q=q_xarr)
s_yarr = mdqf__info(yarr, n=n_yarr, p=p_yarr, q=q_yarr)
s_coef = mdqf__info(coef, n=n_coef, p=p_coef, q=q_coef)
s_wght = mdqf__info(wght, n=n_wght, p=p_wght, q=q_wght)

if ((q_x gt 0l) and (p_coef eq p_x)) then begin
    varr = transpose(coef ## transpose(x))
endif else if ((q_xarr gt 0l) and (p_coef eq (p_xarr+2l)*(p_xarr+1l)/2l)) then begin
    varr = transpose(coef ## transpose(mdqf__matrix(xarr)))
endif else if ((q_yarr gt 0l) and (p_wght eq q_yarr)) then begin
    varr = wght ## yarr
endif else message, /info, "incorrect input (XARR & COEF) / (YARR & WGHT)"

return, varr
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-------------------------------   MDQF__CHOLDC   -----------------------------
;------------------------------------------------------------------------------

function mdqf__choldc, coef, inverse=inverse, status=status, errmsg=errmsg

compile_opt hidden

status = 0
errmsg = ''
chol = [!values.d_nan]
s_coef = mdqf__info(coef, n=n_coef, p=p_coef, q=q_coef, t=t_coef)
p_xarr = long((-3d0 + sqrt(9d0-8d0*(1d0-p_coef)))/2d)

if (p_xarr gt 1l) then begin
    chol = dblarr(p_coef)
    if (keyword_set(inverse)) then begin
        larr = transpose(mdqf__array(coef[0l:(p_xarr+1l)*p_xarr/2l-1l], /upper))
        chol[*] = coef
        chol[0l:(p_xarr+1l)*p_xarr/2l-1l] = mdqf__array(larr ## transpose(larr) / (identity(p_xarr) + 1d), /upper)
    endif else begin
        sylvester = mdqf__sylvester((hessian = mdqf__hessian(coef, linear=linear)), semidef=semidef)
        if (sylvester eq 1) then begin
            chol[*] = coef
            la_choldc, hessian, /double, status=status_choldc, /upper
            if (status_choldc ne 0) then message, /info, 'LA_CHOLDC status: ' + strtrim(status_choldc,2)
            chol[0l:(p_xarr+1l)*p_xarr/2l-1l] = mdqf__array(hessian, /upper)
        endif else message, /info, "incorrect COEF (Sylvester's criterion)"
    endelse
endif else if (p_xarr eq 1l) then begin
    chol[*] = coef
endif else message, /info, "incorrect input COEF size"

return, chol
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;---------------------------   MDQF__MPFIT__PARAMETERS   ----------------------
;------------------------------------------------------------------------------

function mdqf__mpfit__parameters, p, x, y, idx_null=idx_null, idx_eql=idx_eql, idx_par=idx_par, n_idx_par=n_idx_par, clear=clear, status=status, errmsg=errmsg

compile_opt hidden
common common__mdqf__mpfit__constraint, c_x, c_y, c_f

status = 0
errmsg = ''
idx_par = -1l
n_idx_par = 0l

num_par = n_params()
if ((keyword_set(clear)) or (num_par eq 0l)) then begin
    if (n_elements(c_x) ne 0l) then tmp = temporary(c_x)
    if (n_elements(c_y) ne 0l) then tmp = temporary(c_y)
    if (n_elements(c_f) ne 0l) then tmp = temporary(c_f)
    return, 0
endif else if (num_par eq 1l) then begin
    idx_coef = where(c_f eq 0, n_idx_coef, comp=idx_par, ncomp=n_idx_par)
    if ((n_idx_coef gt 0l) and (n_idx_par gt 0l)) then begin
        c = mdqf__choldc(p, /inverse)

        idx_fpar = where(c_f[idx_par] eq 1, n_idx_fpar)
        if (n_idx_fpar gt 0l) then c[idx_par[idx_fpar]] = 0d

        idx_fpar = where(c_f[idx_par] eq 2, n_idx_fpar)
        if (n_idx_fpar gt 0l) then begin
            idx_fpar = idx_par[idx_fpar]
            c[idx_fpar] = (n_idx_fpar eq 1l)? (c_y - c ## transpose(c_x)) / c_x[idx_fpar] : $
                (transpose(la_linear_equation(c_x[idx_fpar,*], c_y - transpose(c[idx_coef] ## transpose(c_x[idx_coef,*])), /double)))
        endif

        return, mdqf__choldc(c)
    endif else begin
        if (n_idx_par gt 0l) then message, /info, "incorrect constraints"
        return, p
    endelse
endif else if (num_par eq 3l) then begin
    n_idx_null = n_elements(idx_null)
    if (n_idx_null eq 1l) then if (idx_null eq -1l) then n_idx_null = 0l
    n_idx_eql = n_elements(idx_eql)
    if (n_idx_eql eq 1l) then if (idx_eql eq -1l) then n_idx_eql = 0l
    if ((n_idx_null gt 0l) or (n_idx_eql gt 0l)) then begin
        f = intarr(n_elements(p))
        if (n_idx_null gt 0l) then f[idx_null] = 1
        if (n_idx_eql gt 0l) then begin
            idx_coef = where(f eq 0, n_idx_coef)
            if (n_idx_coef gt 0l) then begin
                c_x = x[*,idx_eql]
                c_y = y[0l,idx_eql]
                tmp = ortho_gsch(transpose(reverse(c_x[idx_coef,*])), /double, idx=idx_basis)
                f[idx_coef[n_idx_coef-1l-idx_basis]] = 2
            endif else message, /info, "incorrect constraints"
        endif
        c_f = temporary(f)
        idx_par = where(c_f ne 0l, n_idx_par)
    endif
    return, 0
endif else message, /info, "incorrect input parameters"

return, !values.d_nan
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;---------------------------   MDQF__MPFIT__FITFUNC   -------------------------
;------------------------------------------------------------------------------

function mdqf__mpfit__fitfunc, p, x=x, y=y

compile_opt hidden

return, transpose(mdqf__values(x, coef=mdqf__choldc(p, /inverse)) - y)
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-------------------------------   MDQF__MPFIT   ------------------------------
;------------------------------------------------------------------------------

function mdqf__mpfit, c, x, y, idx_null=idx_null, idx_eql=idx_eql, status=status, errmsg=errmsg 

compile_opt hidden

status = 0
errmsg = ''
s_c = mdqf__info(c, n=n_c)
parinfo = replicate({value: 0d, tied: ''}, n_c)
parinfo.value = mdqf__choldc(c)

n_idx_null = n_elements(idx_null)
if (n_idx_null eq 1l) then if (idx_null eq -1l) then n_idx_null = 0l
n_idx_eql = n_elements(idx_eql)
if (n_idx_eql eq 1l) then if (idx_eql eq -1l) then n_idx_eql = 0l

if (n_idx_null + n_idx_eql gt 0l) then begin
    if (n_idx_null + n_idx_eql lt n_c) then begin
        tmp = mdqf__mpfit__parameters(parinfo.value, x, y, idx_null=idx_null, idx_eql=idx_eql, idx_par=idx_par, n_idx_par=n_idx_par)
        for i_idx_par=0l, n_idx_par-1l do $
            parinfo[idx_par[i_idx_par]].tied = (i_idx_par eq 0l)? $
                '((mdqf__mpfit__par = mdqf__mpfit__parameters(p)))['+strtrim(idx_par[i_idx_par],2)+']' : $
                'mdqf__mpfit__par['+strtrim(idx_par[i_idx_par],2)+']'
    endif else message, /info, "incorrect number of conditions (fixations/constraints)"
endif

p = mpfit('mdqf__mpfit__fitfunc', parinfo=parinfo, functargs={x: x, y: y}, /quiet, status=mpfit_status, errmsg=mpfit_errmsg)
tmp = mdqf__mpfit__parameters(/clear)
if (n_elements(mpfit_status) eq 1) then if (mpfit_status lt 1l) then message, /info, "MDQF MPFIT : " + strtrim(mpfit_status,2) + "   (" + strtrim(mpfit_errmsg,2) + ")"

return, mdqf__choldc(p, /inverse)
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;---------------------------   MDQF__COEFFICIENTS   ---------------------------
;------------------------------------------------------------------------------

function mdqf__coefficients, xarr=xarr, yarr=yarr, warr=warr, farr=farr, idx=idx, $
    posdef=posdef, semidef=semidef, status=status, errmsg=errmsg

compile_opt hidden
;------------------------------------------------------------------------------
; INPUT:

status = 0l
errmsg = ''
coef = [!values.d_nan]
s_xarr = mdqf__info(xarr, n=n_xarr, p=p_xarr, q=q_xarr)
if (n_xarr eq 0l) then begin
    message, /info, (errmsg = "(status: "+strtrim((status=1),2)+"): undefined XARR")
    return, coef
endif else coef = dblarr((p_coef = (p_xarr+2l)*(p_xarr+1l)/2l)) + !values.d_nan
s_yarr = mdqf__info(yarr, n=n_yarr, p=p_yarr, q=q_yarr)
if ~((p_yarr eq 1l) and (q_yarr eq q_xarr)) then begin
    message, /info, (errmsg = "(status: "+strtrim((status=1),2)+"): undefined/incorrect YARR")
    return, coef
endif

s_warr = mdqf__info(warr, n=n_warr, p=p_warr, q=q_warr)
if (n_warr ne 0l) then begin
    if ~((p_warr eq 1l) and (q_warr eq q_xarr)) then begin
        message, /info, (errmsg = "(status: "+strtrim((status=1),2)+"): incorrect WARR")
        return, coef
    endif
    w = warr
endif else w = dblarr(1,q_xarr) + 1d
s_w = mdqf__info(w, n=n_w, p=p_w, q=q_w)

s_farr = mdqf__info(farr, n=n_farr, p=p_farr, q=q_farr)
if (n_farr ne 0l) then begin
    if ~((p_farr eq p_coef) and (q_farr eq 1l)) then begin
        message, /info, (errmsg = "(status: "+strtrim((status=1),2)+"): incorrect FARR")
        return, coef
    endif
    f = farr
endif else f = intarr(p_coef) + 1
s_f = mdqf__info(f, n=n_f, p=p_f, q=q_f)
idx_null = where(f eq 0, n_idx_null)

s_idx = mdqf__info(idx, n=n_idx)
;______________________________________________________________________________
;------------------------------------------------------------------------------
; FINITE:

n_idx_eql = 0l
n_idx_fin = 0l
if ((p_xarr gt 0l) and (q_xarr gt 0l) and (p_yarr eq 1l) and (q_yarr eq q_xarr)) then begin
    tmp = bytarr(1l,q_xarr) + 1b
    tmp[0l,*] *= product(finite(xarr), 1)
    tmp[0l,*] *= finite(yarr)
    if ((p_w eq 1l) and (q_w eq q_xarr)) then tmp[0l,*] *= finite(w)
    idx_fin = where(tmp eq 1, n_idx_fin)
    if (n_idx_fin ne q_xarr) then message, /info, "there are undefined values XARR/YARR/WARR"
    if (n_idx_fin gt 0l) then begin
        if (n_idx gt 0l) then begin
            tmp[0l,*] = 0
            tmp[0l,idx] = 1
            idx_eql = where(tmp[0l,idx_fin] eq 1, n_idx_eql)
            if (n_idx_eql ne n_idx) then message, /info, "infinite values in IDX"
        endif
    endif else message, /info, "undefined input XARR/YARR/WARR"
endif

idx_mtx = where(f, n_idx_mtx)
;______________________________________________________________________________
;------------------------------------------------------------------------------
; COEFFICIENTS:

if ((n_idx_mtx gt 0l) and (n_idx_fin ge n_idx_mtx)) then begin
    x = mdqf__matrix(xarr[*,idx_fin],f)
    y = yarr[0l,idx_fin]
    s_x = mdqf__info(x, n=n_x, p=p_x, q=q_x)
    if ((p_w eq 1l) and (q_w eq q_xarr)) then begin
        y *= w[0l,idx_fin]
        for i_x=0l, p_x-1l do x[i_x,*] *= w[0l,idx_fin]
    endif

    x_basis = ortho_gsch(x, n_idx=n_x_basis)
    tmp = ortho_gsch(x[*,idx_eql], idx=idx_eql_basis, n_idx=n_idx_eql_basis)
    if (n_idx_eql_basis lt n_idx_eql) then begin
        message, /info, "degeneracy constraints ("+strtrim(n_idx_eql,2)+" -> "+strtrim(n_idx_eql_basis,2)+")"
        idx_eql = idx_eql[idx_eql_basis]
        n_idx_eql = n_idx_eql_basis
    endif

    if (n_x_basis ge p_x) then begin
        coef[*] = 0d
        coef[idx_mtx] = (n_idx_eql gt 0l)? $
            transpose(bounded_least_squares(x, y, add_a1mat=x[*,idx_eql], add_b1vec=transpose(y[*,idx_eql]), /double, /quiet)) : $
            transpose(bounded_least_squares(x, y, /double, /quiet))

        ;(c); check sylvester criterion and calculate positive definite quadratic form
        sylvester = mdqf__sylvester((hessian = mdqf__hessian(coef, linear=linear)), semidef=semidef)
        if (keyword_set(posdef) and (sylvester ne 1)) then begin
            idx_bnd = (p_xarr gt 1l)? [0l,total(reverse(lindgen(p_xarr-1l)+2l),/cumulative,/int)] : 0l
            idx_mtx_tr = [idx_bnd, lindgen(p_xarr+1)+(p_xarr+1l)*p_xarr/2l]
            n_idx_mtx_tr = n_elements(idx_mtx_tr)
            f_tr = intarr(p_coef)
            f_tr[idx_mtx_tr] = 1 ;(c); diagonal mask + shifts + const
            f_tr *= f ;(c); extra use input mask
            idx_mtx_tr = where(f_tr eq 1, n_idx_mtx_tr)

            ;(c); constraints for coefficients
            bnd = dblarr(2l,p_coef)
            bnd[1l,*] = !values.d_infinity
            bnd[0l,*] = -!values.d_infinity
            bnd[0l,idx_bnd] = 0d
            bnd = bnd[*,idx_mtx_tr]

            hessian_tr = ortho_transform(hessian, transform=tr) ;(c); orthogonal transform for hessian matrix
            x_tr = mdqf__matrix(mdqf__transform(xarr=xarr[*,idx_fin],transform=tr), f_tr) ;(c); points coordinates rotation
            if ((p_w eq 1l) and (q_w eq q_xarr)) then for i_x=0l, total(f_tr,/int)-1l do x_tr[i_x,*] *= w[0l,idx_fin]

            ;(c); calculate initial guess for COEF
            coef_bls = dblarr(p_coef)
            coef_bls[idx_mtx_tr] = (n_idx_eql gt 0l)? $
                transpose(bounded_least_squares(x_tr, y, bnd, add_a1mat=x_tr[*,idx_eql], add_b1vec=transpose(y[*,idx_eql]), /double, /quiet, status=bls_status)) : $
                transpose(bounded_least_squares(x_tr, y, bnd, /double, /quiet, status=bls_status))

            sylvester_bls = mdqf__sylvester((hessian_bls = mdqf__hessian(coef_bls, linear=linear_bls)), semidef=semidef)
            ;(c); epsilon-correction for positive-definite hessian matrix
            if (sylvester_bls ne 1) then begin
                epsilon = max(abs(coef_bls[idx_bnd]),/nan) * 1d-3
                for i_idx_bnd=0l, n_elements(idx_bnd)-1l do if (coef_bls[idx_bnd[i_idx_bnd]] lt epsilon) then coef_bls[idx_bnd[i_idx_bnd]] = epsilon
                sylvester_bls = mdqf__sylvester((hessian_bls = mdqf__hessian(coef_bls, linear=linear_bls)), semidef=semidef)
            endif

            coef_bls = mdqf__transform(coef=coef_bls,tr=tr,/inv) ;(c); reverse transform for coefficients from positive-definite hessian matrix

            x = mdqf__matrix(xarr[*,idx_fin])
            if ((p_w eq 1l) and (q_w eq q_xarr)) then for i_x=0l, p_x-1l do x[i_x,*] *= w[0l,idx_fin]

            ;(c); calculate coefficients positive definite quadratic form
            coef[*] = mdqf__mpfit(coef_bls, x, y, idx_null=idx_null, idx_eql=idx_eql)

;(+);;(!);;(c); need update:
;(+);            ;(c); check and correct degeneracy
;(+);            hessian_tr = ortho_transform((hessian = mdqf__hessian(coef, linear=linear)), transform=tr)
;(+);            idx_xdeg = where(abs(diag_matrix(hessian_tr)) gt max(abs(diag_matrix(hessian_tr))) * 1d-3, n_idx_xdeg)
;(+);            if ((n_idx_xdeg gt 1l) and (n_idx_xdeg lt p_xarr)) then begin
;(+);                message, /info, (errmsg = "(status: "+strtrim((status=-1),2)+"): degeneracy parameners, lowering the rank of a matrix ("+strtrim(p_xarr,2)+" -> "+strtrim(n_idx_xdeg,2)+")")
;(+);                tmp_xarr = xarr[*,0]
;(+);                tmp_xarr[*] = 0
;(+);                tmp_xarr[idx_xdeg] = 1
;(+);                idx_coef = where(mdqf__matrix(tmp_xarr, farr) eq 1, n_idx_coef)
;(+);            
;(+);                tmp_xarr = (mdqf__transform(xarr=xarr, tr=tr))[idx_xdeg,*]
;(+);                if (n_idx gt 0l) then begin
;(+);                    tmp = ortho_gsch(mdqf__matrix(tmp_xarr[*,idx], (n_farr gt 0l)? farr[idx_coef] : undef), /double, /normalization, idx=idx_constr, n_idx=n_idx_constr)
;(+);                endif else n_idx_constr = 0l
;(+);            
;(+);                coef[*] = 0.
;(+);                coef[idx_coef] = mdqf__coefficients(xarr=tmp_xarr, yarr=yarr, warr=warr, farr=(n_farr gt 0l)? farr[idx_coef] : undef, idx=(n_idx_constr gt 0l)? idx_constr : undef, posdef=posdef, semidef=semidef)
;(+);                coef = mdqf__transform(coef=coef, tr=tr, /invers)
;(+);            endif
        endif
    endif else message, /info, (errmsg = "(status: "+strtrim((status=2),2)+"): it is impossible to select a basis for calculating a solution")
endif else message, /info, (errmsg = "(status: "+strtrim((status=1),2)+"): incorrect input XARR/YARR/WARR/FARR (size/finite)")
;______________________________________________________________________________
return, coef
end
;______________________________________________________________________________

;==============================================================================
;==================================   MDQF   ==================================
;==============================================================================

function mdqf, $
    action=action, $ ;(c); select of 'matrix'/'derive'/'hessian'/'sylvester'/'transform'/'minimum'/'weights'/'values'/'coefficients'/''/!null
    xarr=xarr, $ ;(c); input matrix [p,q] P coordinates for Q points
    yarr=yarr, $ ;(c); input matrix [1,q] function values for Q points
    warr=warr, $ ;(c); input matrix [1,q] weights for Q points
    farr=farr, $ ;(c); input matrix [(p+2l)*(p+1)/2] flags
    carr=carr, $ ;(c); input matrix [(p+2l)*(p+1)/2] coefficients
    parr=parr, $ ;(c); input matrix [q] weights for XARR to XMIN/XPOS
    tarr=tarr, $ ;(c); input transform matrix [p,p] (for diagonalization hessian)
    idx=idx,   $ ;(c); input points indices for constraints
    xpos=xpos, $ ;(c); input matrix [p] for calculation in this point
    xdeg=xdeg, $ ;(c); input reference point (coordinates) for removal of degeneracy
    posdef=posdef, semidef=semidef, inverse=inverse, $ ;(c); keywords for __coefficints(), __sylvester(), __choldc(), __minimum() __transform() functions
    coefficients=coef, weights=wght, hessian=hessian, linear=linear, sylvester=sylvester, transform=transform, $ ;(c); output keywords
    xmin=xmin, exmin=exmin, ymin=ymin, eymin=eymin, $ ;(c); output keywords
    status=status, errmsg=errmsg ;(c); status and error message output keywords

;------------------------------------------------------------------------------
; INPUT/OUTPUT/KEYWORDS:

status = 0
errmsg = ''
coef = (wght = (hessian = (linear = (transform = (xmin = (exmin = (ymin = (eymin = [(sylvester = !values.d_nan)]))))))))

s_carr = mdqf__info(carr, n=n_carr, p=p_carr, q=q_carr)
if (n_carr gt 0l) then coef = carr

s_parr = mdqf__info(parr, n=n_parr, p=p_parr, q=q_parr)
if (n_parr gt 0l) then wght = parr

s_tarr = mdqf__info(tarr, n=n_tarr, p=p_tarr, q=q_tarr)
if (n_tarr gt 0l) then transform = tarr

act = ['matrix','derive','hessian','sylvester','transform','minimum','weights','values','choldc','coefficients','']
s_action = mdqf__info(action, n=n_action, t=t_action)
if (n_action eq 1) then begin
    if (t_action eq 7l) then begin
        if (action ne '') then if (total(strmatch(act,action+'*'),/int) ne 1l) then message, (errmsg = "(status: "+strtrim((status=1),2)+"): incorrect ACTION ("+strjoin("'"+strtrim(act,2)+"'",',')+",!null)")
    endif else message, (errmsg = "(status: "+strtrim((status=1),2)+"): incorrect ACTION ("+strjoin("'"+strtrim(act,2)+"'",',')+",!null)")
endif else action = ''
;______________________________________________________________________________
;------------------------------------------------------------------------------
; ACTIONS:

if (strmatch(action,'mat*')) then begin
    return, mdqf__matrix(xarr, farr, status=status, errmsg=errmsg)
endif else if (strmatch(action,'der*')) then begin
    return, mdqf__derive(xarr, farr, status=status, errmsg=errmsg)
endif else if (strmatch(action,'hes*')) then begin
    return, (hessian = mdqf__hessian(coef, linear=linear, status=status, errmsg=errmsg))
endif else if (strmatch(action,'syl*')) then begin
    return, (sylvester = mdqf__sylvester((hessian = mdqf__hessian(coef, linear=linear)), semidef=semidef, status=status, errmsg=errmsg))
endif else if (strmatch(action,'tr*')) then begin
    if (n_carr eq 0l) then tmp = temporary(coef)
    if (n_tarr eq 0l) then tmp = temporary(transform)
    return, mdqf__transform(xarr=xarr, coef=coef, transform=transform, inverse=inverse)
endif else if (strmatch(action,'min*')) then begin
    return, mdqf__minimum(coef, xdeg=xdeg, xpos=xpos, xmin=xmin, exmin=exmin, ymin=ymin, eymin=eymin, hessian=hessian, linear=linear, sylvester=sylvester, status=status, errmsg=errmsg)
endif else if (strmatch(action,'wei*')) then begin
    return, mdqf__weights(mdqf__matrix(xarr, farr), mdqf__matrix(xpos, farr), idx=idx, status=status, errmsg=errmsg)
endif else if (strmatch(action,'val*')) then begin
    return, mdqf__values(xarr=xarr, coef=coef, yarr=yarr, weights=wght, status=status, errmsg=errmsg)
endif else if (strmatch(action,'ch*')) then begin
    return, mdqf__choldc(coef, inverse=inverse, status=status, errmsg=errmsg)
endif else begin
    coef = mdqf__coefficients(xarr=xarr, yarr=yarr, warr=warr, farr=farr, idx=idx, posdef=posdef, semidef=semidef, status=status, errmsg=errmsg)
    if (status le 0l) then if (arg_present(hessian) or arg_present(linear) or arg_present(sylvester)) then hessian = mdqf__hessian(coef, linear=linear, status=status, errmsg=errmsg)
    if (status le 0l) then if (arg_present(sylvester)) then sylvester = mdqf__sylvester(hessian, semidef=semidef, status=status, errmsg=errmsg)
    if (status le 0l) then if (arg_present(xmin) or arg_present(exmin) or arg_present(ymin) or arg_present(eymin) or arg_present(wght)) then begin
        xmin = mdqf__minimum(coef, xdeg=xdeg, exmin=exmin, ymin=ymin, eymin=eymin, status=status, errmsg=errmsg)
    endif
    if (status le 0l) then if (arg_present(wght)) then wght = mdqf__weights(mdqf__matrix(xarr, farr), mdqf__matrix(xmin, farr), idx=idx, status=status, errmsg=errmsg)
    if (status ne 0l) then message, /info, errmsg
    return, coef
endelse
;______________________________________________________________________________
message, /info, (errmsg = "(status: "+strtrim((status=1),2)+"): undefined/incorrect ACTION ("+strjoin("'"+strtrim(act,2)+"'",',')+",!null)")
return, !values.d_nan
end
;______________________________________________________________________________
