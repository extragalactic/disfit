;------------------------------------------------------------------------------
;+
; FILENAME: normvec.pro
;
;
;
; EXAMPLE:
;   1)  IDL> print, normvec([[1d,2d],[-1d,1d]], /norm)
;   2)  IDL> qhull, x, tri
;       IDL> normals = normvec(x, tri=tri, /debug)
;
; VERSION: 1.0.200428
;-
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-------------------------   NORMVEC__COEFFICIENTS   --------------------------
;------------------------------------------------------------------------------

function normvec__coefficients, points, normalize=normalize, debug=debug

compile_opt hidden

s_pnt = size(points)
d_pnt = (s_pnt[0] gt 0l)? s_pnt[1] : (s_pnt[s_pnt[0]+2] eq 1l)? 1l : 0l
n_pnt = (d_pnt gt 0l)? (s_pnt[0] eq 2l)? s_pnt[2] : (s_pnt[0] lt 2l)? 1l : 0l : 0l

normal = (d_pnt gt 0l)? dblarr(d_pnt+1l) + !values.d_nan : !values.d_nan
if ((d_pnt gt 1l) and (n_pnt eq d_pnt)) then begin
    matrix = dblarr(d_pnt+1l,d_pnt+1l)
    matrix[d_pnt,1:*] = 1d
    matrix[*,0] = (-1d)^(dindgen(d_pnt+1l))
    matrix[0:d_pnt-1l,1:*] = points
    ind_gen = lindgen(d_pnt+1l)
    for i_pnt=0l, d_pnt do normal[i_pnt] = matrix[i_pnt,0] * la_determ(matrix[where(ind_gen ne i_pnt),1:*], /check, /double)
    if (keyword_set(normalize)) then if ((normsqr = total(normal[0:d_pnt-1l]*normal[0:d_pnt-1l], /double)) gt 0d) then normal /= sqrt(normsqr)
endif
if (keyword_set(debug)) then print, normal, format='("NORMVEC:",tr3,'+strtrim(d_pnt+1l,2)+'f)'

return, normal
end
;______________________________________________________________________________

;==============================================================================
;=================================   NORMVEC   ================================
;==============================================================================

function normvec, points, triangulation=tri, normalize=normalize, nouniq=nouniq, debug=debug

s_pnt = size(points)
d_pnt = (s_pnt[0] gt 0l)? s_pnt[1] : (s_pnt[s_pnt[0]+2] eq 1l)? 1l : 0l
n_pnt = (d_pnt gt 0l)? (s_pnt[0] eq 2l)? s_pnt[2] : (s_pnt[0] lt 2l)? 1l : 0l : 0l

s_tri = size(tri)
d_tri = (s_tri[0] gt 0l)? s_tri[1] : (s_tri[s_tri[0]+2] eq 1l)? 1l : 0l
n_tri = (d_tri gt 0l)? (s_tri[0] eq 2l)? s_tri[2] : (s_tri[0] lt 2l)? 1l : 0l : 0l

normals = (d_pnt gt 0l)? dblarr(d_pnt+1l) + !values.d_nan : !values.d_nan
if ((d_pnt gt 1l) and (n_pnt ge d_pnt)) then begin
    if ((n_tri gt 0l) and (d_tri eq d_pnt)) then begin
        normals = dblarr(d_pnt+1l,n_tri)
        for i_tri=0l, n_tri-1l do normals[*,i_tri] = normvec__coefficients(points[*,tri[*,i_tri]], normalize=normalize, debug=debug)
        if (~keyword_set(nouniq)) then begin
            norms = sqrt(total(normals[0l:d_pnt-1l,*]^2d, 1, /double))
            tmp_normals = normals
            for i_tri=0l, n_tri-1l do if ((finite(norms[i_tri])) and (norms[i_tri] gt 0d)) then tmp_normals[*,i_tri] /= norms[i_tri]
            ind_uniq = uniq_vectors(tmp_normals, multisort_array(transpose(tmp_normals)))
            normals = normals[*,ind_uniq]
        endif
    endif else if (n_pnt eq d_pnt) then begin
        normals = normvec__coefficients(points, normalize=normalize, debug=debug)
    endif else message, /info, 'incorrect input POINTS/TRIANGULATION'
endif else message, /info, 'incorrect input POINTS'

return, normals
end
;______________________________________________________________________________
