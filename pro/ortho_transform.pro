;------------------------------------------------------------------------------
;+
; FUNCTION: ortho_transform.pro
;
; DESCRIPTION: create matrix for orthogonal transformation and return diagonal matrix
;
; INPUT:
;   matrix -- symmetric matrix (array_equal(transpose(matrix),matrix))
;
; OUTPUT:
;   result -- diagonal matrix of orthogonal transformation
;
; OUTPUT KEYWORDS:
;   transform -- square orthogonal transformation matrix
;
; EXAMPLE:
;   1)  IDL> dg = ortho_transform((mtx = [[2d,2d,-2d],[2d,5d,-4d],[-2d,-4d,5d]]), tr=tr)
;       IDL> mtx_tr = matrix_multiply(tr, mtx # tr, /atranspose) ;(c); orthogonal transform, mtx_tr == dg
;       IDL> dg_rev = matrix_multiply(tr # dg, tr, /btranspose)  ;(c); reverse orthogonal transform, dg_rev == mtx
;   2)  IDL> dg = ortho_transform((mtx = [[2d,2d,-2d],[2d,5d,-4d],[-2d,-4d,5d]]), tr=tr)
;       IDL> vec_tr = matrix_multiply(tr, (tmp = [1d,2d,-1d])) ;(c); rotate row vector
;       IDL> vec = matrix_multiply(tr, vec_tr, /atranspose) ;(c); reverse rotate row vector, vec == tmp
;   3)  IDL> dg = ortho_transform((mtx = [[2d,2d,-2d],[2d,5d,-4d],[-2d,-4d,5d]]), tr=tr)
;       IDL> vec_tr = transpose(matrix_multiply(tr, (tmp = transpose([1d,2d,-1d])), /btranspose)) ;(c); rotate column vector
;       IDL> vec = transpose(matrix_multiply(tr, vec_tr, /atranspose, /btranspose)) ;(c); reverse rotate colunm vector, vec == tmp
;
; NOTE:
;   AB -- A ## TRANSPOSE(B)
;   A # B is MATRIX_MULTIPLY(A, B).
;   TRANSPOSE(A) # B is MATRIX_MULTIPLY(A, B, /ATRANSPOSE).
;   A # TRANSPOSE(B) is MATRIX_MULTIPLY(A, B, /BTRANSPOSE).
;   TRANSPOSE(A) # TRANSPOSE(B) is MATRIX_MULTIPLY(A, B, /ATRANSPOSE, /BTRANSPOSE).
;   MATRIX_MULTIPLY can also be used in place of the ## operator.
;   For example, A ## B is equivalent to MATRIX_MULTIPLY(B, A), and A ## TRANSPOSE(B) is equivalent to MATRIX_MULTIPLY(B, A, /ATRANSPOSE).
;
; VERSION: 1.0.201227
;-
;______________________________________________________________________________

;==============================================================================
;=============================   ORTHO_TRANSFORM   ============================
;==============================================================================

function ortho_transform, matrix, transform=transform

compile_opt hidden

diagonal = (transform = [!values.d_nan])

s_matrix = size(matrix)
d_matrix = s_matrix[0]
t_matrix = s_matrix[s_matrix[0]+1l]
n_matrix = s_matrix[s_matrix[0]+2l]
p_matrix = (s_matrix[0] gt 0l)? s_matrix[1] : (n_matrix eq 1l)? 1l : 0l
q_matrix = (s_matrix[0] gt 1l)? s_matrix[2] : ((p_matrix gt 0l) and (n_matrix eq p_matrix))? 1l : 0l

if ((n_matrix gt 1l) and (d_matrix eq 2) and (p_matrix eq q_matrix)) then begin
    eigenvalues = la_eigenproblem(matrix, eigenvectors=eigenvectors, norm_balance=norm_balance, /double)
    transform = ortho_gsch(eigenvectors, /double, /normalization)
    diagonal = matrix_multiply(transform, matrix # transform, /atranspose)
    if ((t_matrix eq 4) or (t_matrix eq 5)) then begin
        transform = double(transform)
        diagonal = double(diagonal)
    endif
endif

return, diagonal
end
;______________________________________________________________________________
