;------------------------------------------------------------------------------
;+
; FILENAME: disfit.pro
;
; EXAMPLE:
;
;   1)  IDL> xmin = x[*,disfit((x = randomu(seed, 2, 100)), funcname='total')]
;
; VERSION: 2.0.210000
;-
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-----------------------------   DISFIT__COMMON  ------------------------------
;------------------------------------------------------------------------------

function disfit__common, idx_inp, p_pnt_arr=key_p_pnt_arr, q_pnt_arr=key_q_pnt_arr, pnt_arr=key_pnt_arr, $
    parinfo=key_parinfo, idx_dis=key_idx_dis, ncalls=key_ncalls, func_val=key_func_val, par_val=key_par_val, par_err=key_par_err

compile_opt hidden
common common__disfit__qhull, p_pnt_arr, q_pnt_arr, c_pnt_arr, c_conn_arr, c_tri_arr, c_norm_arr
common common__disfit__funcs, c_func_name, c_conn_depth, c_frame_conn_depth, c_frame_range, c_minrange, c_maxrange, c_parinfo, c_idx_dis, c_ncalls, c_idx_min, c_func_val, c_func_flag, c_par_val, c_par_err

if (keyword_set(key_p_pnt_arr)) then begin
    return, (n_elements(p_pnt_arr) eq 1l)? p_pnt_arr : 0l
endif else if (keyword_set(key_q_pnt_arr)) then begin
    return, (n_elements(q_pnt_arr) eq 1l)? q_pnt_arr : 0l
endif else if (keyword_set(key_pnt_arr)) then begin
    return, (n_elements(idx_inp) gt 0l)? c_pnt_arr[*,idx_inp] : c_pnt_arr
endif else if (keyword_set(key_parinfo)) then begin
    return, (n_elements(c_parinfo) eq 1l)? c_parinfo : 0l
endif else if (keyword_set(key_idx_dis)) then begin
    return, (n_elements(c_idx_dis) gt 0l)? c_idx_dis : -1l
endif else if (keyword_set(key_ncalls)) then begin
    return, (n_elements(c_ncalls) eq 1l)? c_ncalls : 0l
endif else if (keyword_set(key_func_val)) then begin
    return, (n_elements(idx_inp) gt 0l)? c_func_val[0l,idx_inp] : c_func_val
endif else if (keyword_set(key_par_val)) then begin
    return, (n_elements(c_par_val) gt 0l)? (n_elements(idx_inp) gt 0l)? c_par_val[*,idx_inp] : c_par_val : !values.d_nan
endif else if (keyword_set(key_par_err)) then begin
    return, (n_elements(c_par_err) gt 0l)? (n_elements(idx_inp) gt 0l)? c_par_err[*,idx_inp] : c_par_err : !values.d_nan
endif

return, 0
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;------------------------------   DISFIT__INFO   ------------------------------
;------------------------------------------------------------------------------

function disfit__info, inp, s_=s_inp, d_=d_inp, t_=t_inp, n_=n_inp, p_=p_inp, q_=q_inp, k_=k_inp, $
    integer=integer, finite=finite, idx=idx, tag_names=tag_names

compile_opt strictarr, hidden

key = 1
d_inp = (t_inp = (n_inp = (p_inp = (q_inp = (k_inp = 0l)))))
if (n_elements(s_inp) eq 0l) then s_inp = size(inp)
if (s_inp[0] gt 0) then key = (product(s_inp[1:s_inp[0]]) eq s_inp[s_inp[0]+2l])
if ~(keyword_set(key)) then begin
    message, /info, "undefined/incorrect INPUT/SIZE(INPUT)"   
    return, [0l,0l,0l]
endif

d_inp = s_inp[0]
t_inp = s_inp[s_inp[0]+1l]
n_inp = s_inp[s_inp[0]+2l]
p_inp = (s_inp[0] gt 0l)? s_inp[1] : (n_inp eq 1l)? 1l : 0l
q_inp = (s_inp[0] gt 1l)? s_inp[2] : ((p_inp gt 0l) and (n_inp eq p_inp))? 1l : 0l
k_inp = (~array_equal(s_inp,[0l,0l,0l]) and (s_inp[0] le 2))

if (keyword_set(k_inp) and keyword_set(finite)) then k_inp = (product(finite(inp),/int) ne 0)
if (keyword_set(k_inp) and (keyword_set(integer) or (n_elements(idx) eq 1))) then k_inp = array_equal(inp,floor(inp))
if (keyword_set(k_inp) and (n_elements(idx) eq 1)) then begin
    idx_inp = where(~((inp ge 0l) and (inp lt idx)), n_idx_inp)
    k_inp = (n_idx_inp eq 0l)
endif
if (keyword_set(k_inp) and ((n_tag_names = n_elements(tag_names)) gt 0l)) then begin
    if ((t_inp eq 8) and (size(tag_names,/type) eq 7)) then begin
        inp_tags = strtrim(tag_names(inp[0]),2)
        for i_tag=0l, n_tag_names-1l do if (keyword_set(k_inp)) then k_inp = (total(strmatch(inp_tags,strtrim(strupcase(tag_names[i_tag]),2)),/int) eq 1)
    endif else begin
        k_inp = 0
        message, /info, "incorrect input STRUCTURE/TAG_NAMES"
    endelse
endif

return, s_inp
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-----------------------   DISFIT__INFO__CHECK__CYCLE   -----------------------
;------------------------------------------------------------------------------

function disfit__info__check__cycle, val, arr, eq_=eq_, ne_=ne_, ge_=ge_, gt_=gt_, lt_=lt_, le_=le_

compile_opt strictarr, hidden

if ((n_elements(val) eq 1l) and (n_elements(arr) gt 0l)) then begin
    if (keyword_set(eq_)) then begin
        for i=0l, n_elements(arr)-1l do if (key = (val eq arr[i])) then break
    endif else if (keyword_set(ne_)) then begin
        for i=0l, n_elements(arr)-1l do if ~(key = (val ne arr[i])) then break
    endif else if (keyword_set(ge_)) then begin
        key = (val ge max(arr))
    endif else if (keyword_set(gt_)) then begin
        key = (val gt max(arr))
    endif else if (keyword_set(lt_)) then begin
        key = (val lt min(arr))
    endif else if (keyword_set(le_)) then begin
        key = (val le min(arr))
    endif else key = 0
endif else key = 1

return, key
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;---------------------------   DISFIT__INFO__CHECK   --------------------------
;------------------------------------------------------------------------------

function disfit__info__check, inp, $
    s_=s_inp, eq_s_=eq_s_inp, ne_s_=ne_s_inp, $
    d_=d_inp, eq_d_=eq_d_inp, ne_d_=ne_d_inp, ge_d_=ge_d_inp, gt_d_=gt_d_inp, lt_d_=lt_d_inp, le_d_=le_d_inp, $
    t_=t_inp, eq_t_=eq_t_inp, ne_t_=ne_t_inp, ge_t_=ge_t_inp, gt_t_=gt_t_inp, lt_t_=lt_t_inp, le_t_=le_t_inp, $
    n_=n_inp, eq_n_=eq_n_inp, ne_n_=ne_n_inp, ge_n_=ge_n_inp, gt_n_=gt_n_inp, lt_n_=lt_n_inp, le_n_=le_n_inp, $
    p_=p_inp, eq_p_=eq_p_inp, ne_p_=ne_p_inp, ge_p_=ge_p_inp, gt_p_=gt_p_inp, lt_p_=lt_p_inp, le_p_=le_p_inp, $
    q_=q_inp, eq_q_=eq_q_inp, ne_q_=ne_q_inp, ge_q_=ge_q_inp, gt_q_=gt_q_inp, lt_q_=lt_q_inp, le_q_=le_q_inp, $
    k_=k_inp, eq_k_=eq_k_inp, ne_k_=ne_k_inp, ge_k_=ge_k_inp, gt_k_=gt_k_inp, lt_k_=lt_k_inp, le_k_=le_k_inp, $
    _extra=extra

compile_opt strictarr, hidden

key = 1

s_inp = disfit__info(inp, s_=s_inp, d_=d_inp, t_=t_inp, n_=n_inp, p_=p_inp, q_=q_inp, k_=k_inp, _extra=extra)

if ((key eq 1) and (n_elements(eq_s_inp) gt 0l)) then begin
    if (n_elements(s_inp) eq n_elements(eq_s_inp)) then begin
        for i=0l, n_elements(s_inp)-1l do if ~(key = (s_inp[i] eq eq_s_inp[i])) then break
    endif else key = 0
endif

if ((key eq 1) and (n_elements(ne_s_inp) gt 0l)) then begin
    if (n_elements(s_inp) eq n_elements(ne_s_inp)) then begin
        for i=0l, n_elements(s_inp)-1l do if (key = (s_inp[i] ne ne_s_inp[i])) then break
    endif else key = 1
endif

if (key eq 1) then key = disfit__info__check__cycle(d_inp, eq_d_inp, /eq)
if (key eq 1) then key = disfit__info__check__cycle(t_inp, eq_t_inp, /eq)
if (key eq 1) then key = disfit__info__check__cycle(n_inp, eq_n_inp, /eq)
if (key eq 1) then key = disfit__info__check__cycle(p_inp, eq_p_inp, /eq)
if (key eq 1) then key = disfit__info__check__cycle(q_inp, eq_q_inp, /eq)
if (key eq 1) then key = disfit__info__check__cycle(k_inp, eq_k_inp, /eq)

if (key eq 1) then key = disfit__info__check__cycle(d_inp, ne_d_inp, /ne)
if (key eq 1) then key = disfit__info__check__cycle(t_inp, ne_t_inp, /ne)
if (key eq 1) then key = disfit__info__check__cycle(n_inp, ne_n_inp, /ne)
if (key eq 1) then key = disfit__info__check__cycle(p_inp, ne_p_inp, /ne)
if (key eq 1) then key = disfit__info__check__cycle(q_inp, ne_q_inp, /ne)
if (key eq 1) then key = disfit__info__check__cycle(k_inp, ne_k_inp, /ne)

if (key eq 1) then key = disfit__info__check__cycle(d_inp, ge_d_inp, /ge)
if (key eq 1) then key = disfit__info__check__cycle(t_inp, ge_t_inp, /ge)
if (key eq 1) then key = disfit__info__check__cycle(n_inp, ge_n_inp, /ge)
if (key eq 1) then key = disfit__info__check__cycle(p_inp, ge_p_inp, /ge)
if (key eq 1) then key = disfit__info__check__cycle(q_inp, ge_q_inp, /ge)
if (key eq 1) then key = disfit__info__check__cycle(k_inp, ge_k_inp, /ge)

if (key eq 1) then key = disfit__info__check__cycle(d_inp, gt_d_inp, /gt)
if (key eq 1) then key = disfit__info__check__cycle(t_inp, gt_t_inp, /gt)
if (key eq 1) then key = disfit__info__check__cycle(n_inp, gt_n_inp, /gt)
if (key eq 1) then key = disfit__info__check__cycle(p_inp, gt_p_inp, /gt)
if (key eq 1) then key = disfit__info__check__cycle(q_inp, gt_q_inp, /gt)
if (key eq 1) then key = disfit__info__check__cycle(k_inp, gt_k_inp, /gt)

if (key eq 1) then key = disfit__info__check__cycle(d_inp, lt_d_inp, /lt)
if (key eq 1) then key = disfit__info__check__cycle(t_inp, lt_t_inp, /lt)
if (key eq 1) then key = disfit__info__check__cycle(n_inp, lt_n_inp, /lt)
if (key eq 1) then key = disfit__info__check__cycle(p_inp, lt_p_inp, /lt)
if (key eq 1) then key = disfit__info__check__cycle(q_inp, lt_q_inp, /lt)
if (key eq 1) then key = disfit__info__check__cycle(k_inp, lt_k_inp, /lt)

if (key eq 1) then key = disfit__info__check__cycle(d_inp, le_d_inp, /le)
if (key eq 1) then key = disfit__info__check__cycle(t_inp, le_t_inp, /le)
if (key eq 1) then key = disfit__info__check__cycle(n_inp, le_n_inp, /le)
if (key eq 1) then key = disfit__info__check__cycle(p_inp, le_p_inp, /le)
if (key eq 1) then key = disfit__info__check__cycle(q_inp, le_q_inp, /le)
if (key eq 1) then key = disfit__info__check__cycle(k_inp, le_k_inp, /le)

return, key
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;------------------------------   DISFIT__DELETE   ----------------------------
;------------------------------------------------------------------------------

function disfit__delete, inp, flg

compile_opt strictarr, hidden

key = (n_elements(flg) eq 1)? flg : 0
if (n_elements(inp) ne 0l) then tmp = temporary(inp)

return, key
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;------------------------------   DISFIT__QHULL   -----------------------------
;------------------------------------------------------------------------------

function disfit__qhull, pnt_arr=pnt_arr, conn_arr=conn_arr, tri_arr=tri_arr, norm_arr=norm_arr, p_pnt_arr=p_pnt_out, q_pnt_arr=q_pnt_out, $
    pnt_prep_read=pnt_prep_read, pnt_prep_write=pnt_prep_write, rewrite=rewrite, cache=cache, clear=clear

compile_opt hidden
common common__disfit__qhull

status_qhull = 0
p_pnt_out = 0l
q_pnt_out = 0l
if (keyword_set(clear)) then begin
    tmp = disfit__delete(p_pnt_arr)
    tmp = disfit__delete(q_pnt_arr)
    tmp = disfit__delete(c_pnt_arr)
    tmp = disfit__delete(c_conn_arr)
    tmp = disfit__delete(c_tri_arr)
    tmp = disfit__delete(c_norm_arr)
    return, status_qhull
endif

key_cache = 1
if (keyword_set(cache)) then begin
    if (key_cache) then key_cache = disfit__info__check(p_pnt_arr, eq_n=1l)
    if (key_cache) then key_cache = disfit__info__check(q_pnt_arr, eq_n=1l)
    if (key_cache) then key_cache = disfit__info__check(c_pnt_arr, gt_p=0l, gt_q=0l, eq_p=p_pnt_arr, eq_q=q_pnt_arr)
    if (key_cache) then key_cache = disfit__info__check(c_conn_arr, gt_p=0l, eq_q=1l)
    if (key_cache) then key_cache = disfit__info__check(c_tri_arr, eq_p=p_pnt_arr+1l, gt_q=0l)
    if (key_cache) then key_cache = disfit__info__check(c_norm_arr, eq_p=p_pnt_arr+1l, gt_q=0l)
    key_cache = ~key_cache
    if (key_cache eq 1) then tmp = disfit__qhull(/clear)
endif

if ~(disfit__info__check(pnt_prep_read, eq_t=7, eq_n=1)) then pnt_prep_read = ''
if ~(disfit__info__check(pnt_prep_write, eq_t=7, eq_n=1)) then pnt_prep_write = ''

if (keyword_set(key_cache) and keyword_set(pnt_prep_read)) then begin
    if (file_test(pnt_prep_read)) then begin
        c_pnt_arr  = readfits(pnt_prep_read, ext=0, /silent)
        c_conn_arr = readfits(pnt_prep_read, ext=1, /silent)
        c_tri_arr  = readfits(pnt_prep_read, ext=2, /silent)
        c_norm_arr = readfits(pnt_prep_read, ext=3, /silent)
        tmp = disfit__info__check(s=size(c_pnt_arr), p=p_pnt_arr, q=q_pnt_arr, gt_p=0l, gt_q=0l)
        p_pnt_out = p_pnt_arr
        q_pnt_out = q_pnt_arr
        return, status_qhull
    endif else message, /info, "cache common qhull file not found"
endif

if (keyword_set(key_cache) and disfit__info__check(pnt_arr, p=tmp_p_pnt_arr, q=tmp_q_pnt_arr, gt_p=0l, gt_q=0l)) then begin
    p_pnt_arr = temporary(tmp_p_pnt_arr)
    q_pnt_arr = temporary(tmp_q_pnt_arr)
    c_pnt_arr = temporary(pnt_arr)
endif else key_cache = 0

if (keyword_set(key_cache)) then begin
    if (disfit__info__check(conn_arr, gt_p=0l, eq_q=1) and $
        disfit__info__check(tri_arr, gt_p=0l, gt_q=0l, eq_p=p_pnt_arr+1l) and $
        disfit__info__check(norm_arr, gt_p=0l, gt_q=0l, eq_p=p_pnt_arr+1l)) then begin
            c_conn_arr = temporary(conn_arr)
            c_tri_arr = temporary(tri_arr)
            c_norm_arr = temporary(norm_arr)
    endif else begin
        if (p_pnt_arr eq 1l) then begin
            if (q_pnt_arr gt 2l) then begin
                idx_sort_pnt_arr = sort(c_pnt_arr)
                c_conn_arr = lonarr(q_pnt_arr*3l-2l,/nozero)
                c_tri_arr = lonarr(p_pnt_arr+1, q_pnt_arr,/nozero)
                for i_pnt_arr=0l, q_pnt_arr-1l do begin
                    if (i_pnt_arr eq 0l) then i_conn_arr = q_pnt_arr
                    c_conn_arr[i_pnt_arr] = i_conn_arr
                    i_num_tmp = ((i_pnt_arr gt 0l) and (i_pnt_arr lt q_pnt_arr-1l))? 2l : 1l
                    c_conn_arr[i_conn_arr:i_conn_arr+i_num_tmp-1l] = $
                        (i_pnt_arr eq 0l)? idx_sort_pnt_arr[i_pnt_arr+1l] : $
                        (i_pnt_arr eq q_pnt_arr-1l)? idx_sort_pnt_arr[i_pnt_arr-1l] : $
                        idx_sort_pnt_arr[i_pnt_arr+[-1l,1l]]
                    c_tri_arr[*,i_pnt_arr] = idx_sort_pnt_arr[i_pnt_arr + ((i_pnt_arr eq 0l)? [0l,1l] : (i_pnt_arr eq q_pnt_arr-1l)? [-1l,0l] : [-1l,1l])]
                    i_conn_arr += i_num_tmp
                endfor
                c_norm_arr = [[c_pnt_arr[idx_sort_pnt_arr[0]],0d],[c_pnt_arr[idx_sort_pnt_arr[q_pnt_arr-1l]],0d]]
            endif else begin
                c_conn_arr = (q_pnt_arr eq 1l)? [1l,0l] : (q_pnt_arr eq 2l)? [2l,3l,1l,0l] : 0
                c_tri_arr = (q_pnt_arr eq 2l)? [0,1] : 0
                c_norm_arr = (q_pnt_arr eq 2l)? [[0,0],[1,1]] : 0
            endelse
        endif else if (p_pnt_arr gt 1l) then begin
            qhull, c_pnt_arr, c_tri_arr, /delaunay, connectivity=c_conn_arr
            qhull, c_pnt_arr, surf
            tmp_surf = surf
            idx_uniq_surf = uniq(surf,sort(surf))
            for i_idx_uniq_surf=0l, n_elements(idx_uniq_surf)-1l do tmp_surf[where(surf eq surf[idx_uniq_surf[i_idx_uniq_surf]])] = i_idx_uniq_surf
            c_norm_arr = normvec(c_pnt_arr[*,surf[idx_uniq_surf]], triangulation=temporary(tmp_surf))
            (surf = (idx_uniq_surf = 0))
        endif else message, /info, "incorrect input coordinates points"
    endelse
endif

if ~(disfit__info__check(c_pnt_arr, gt_p=0l, gt_q=0l) and $
    disfit__info__check(c_conn_arr, gt_p=0l, eq_q=1) and $
    disfit__info__check(c_tri_arr, gt_p=0l, gt_q=0l, eq_p=((n_elements(p_pnt_arr) eq 1)? p_pnt_arr+1l : 0l)) and $
    disfit__info__check(c_norm_arr, gt_p=0l, gt_q=0l, eq_p=((n_elements(p_pnt_arr) eq 1)? p_pnt_arr+1l : 0l)) $
    ) then status_qhull = 1

if (keyword_set(pnt_prep_write)) then begin   
    if (~file_test(pnt_prep_write) or keyword_set(rewrite)) then begin
        if (status_qhull eq 0) then begin
            writefits, pnt_prep_write, /silent, c_pnt_arr
            writefits, pnt_prep_write, /silent, /append, c_conn_arr
            writefits, pnt_prep_write, /silent, /append, c_tri_arr
            writefits, pnt_prep_write, /silent, /append, c_norm_arr
        endif else message, /info, "undefined/incorrect cache common qhull for writing fits file"
    endif else message, /info, "cache common qhull file already exist"
endif

if (status_qhull eq 0) then begin
    p_pnt_out = p_pnt_arr
    q_pnt_out = q_pnt_arr
endif

return, status_qhull
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-------------------------   DISFIT__NEIGHBOR__DEPTH   ------------------------
;------------------------------------------------------------------------------

function disfit__neighbor__depth, idx_inp, i_idx_inp, n_idx=n_idx_out

compile_opt hidden
common common__disfit__qhull
common common__disfit__funcs

idx_out = -1l
n_idx_out = 0l

if (disfit__info__check(idx_inp, n_=n_idx_inp, gt_n=0l, eq_k=1, idx=q_pnt_arr)) then begin
    idx_out = idx_inp
    n_idx_out = n_idx_inp
    i_idx_out_inp = (disfit__info__check(i_idx_inp, eq_n=1l, idx=n_idx_inp))? i_idx_inp : 0l

    ;(c); select connectivity indices from connectivity matrix
    n_idx_tmp = total(c_conn_arr[idx_out[i_idx_out_inp:*]+1l] - c_conn_arr[idx_out[i_idx_out_inp:*]], /int)
    idx_tmp = lonarr(n_idx_tmp) - 1l
    flag_tmp = intarr(n_idx_tmp) + 1
    k_min = disfit__info__check(c_minrange, eq_p=2, eq_q=p_pnt_arr)
    k_max = disfit__info__check(c_maxrange, eq_p=2, eq_q=p_pnt_arr)

    i_idx_tmp = 0l
    for i_idx_out=i_idx_out_inp, n_idx_out-1l do begin
        if ((d_idx_tmp = c_conn_arr[idx_out[i_idx_out]+1l] - c_conn_arr[idx_out[i_idx_out]]) gt 0l) then begin
            idx_tmp[i_idx_tmp:i_idx_tmp+d_idx_tmp-1l] = c_conn_arr[c_conn_arr[idx_out[i_idx_out]]:c_conn_arr[idx_out[i_idx_out]+1l]-1l]
            ;(c); checking min/max criterion
            if (keyword_set(k_min) or keyword_set(k_max)) then begin
                idx_flag = idx_tmp[i_idx_tmp:i_idx_tmp+d_idx_tmp-1l]
                if (keyword_set(k_min)) then begin
                    for i_pnt_arr=0l, p_pnt_arr-1l do begin
                        idx_flag_tmp = where( $
                            (c_pnt_arr[i_pnt_arr,idx_flag] ge c_pnt_arr[i_pnt_arr,idx_out[i_idx_out]] + c_minrange[0,i_pnt_arr]) and $
                            (c_pnt_arr[i_pnt_arr,idx_flag] le c_pnt_arr[i_pnt_arr,idx_out[i_idx_out]] + c_minrange[1,i_pnt_arr]) $
                            , n_idx_flag_tmp)
                        if (temporary(n_idx_flag_tmp) gt 0l) then flag_tmp[i_idx_tmp+temporary(idx_flag_tmp)] = 0
                    endfor
                endif
                if (keyword_set(k_max)) then begin
                    for i_pnt_arr=0l, p_pnt_arr-1l do begin
                        idx_flag_tmp = where( $
                            (c_pnt_arr[i_pnt_arr,idx_flag] le c_pnt_arr[i_pnt_arr,idx_out[i_idx_out]] + c_maxrange[0,i_pnt_arr]) or $
                            (c_pnt_arr[i_pnt_arr,idx_flag] ge c_pnt_arr[i_pnt_arr,idx_out[i_idx_out]] + c_maxrange[1,i_pnt_arr]) $
                            , n_idx_flag_tmp)
                        if (temporary(n_idx_flag_tmp) gt 0l) then flag_tmp[i_idx_tmp+temporary(idx_flag_tmp)] = 0
                    endfor
                endif
            endif
            i_idx_tmp += d_idx_tmp
        endif
    endfor

    ;(c); uniq indices (with mask)
    idx_sort = sort(flag_tmp) ;(c); for checking any true flag for each index
    idx_uniq = idx_sort[uniq(idx_tmp[idx_sort],sort(idx_tmp[idx_sort]))]
    idx_tmp = idx_tmp[idx_uniq]
    flag_tmp = flag_tmp[idx_uniq]

    ;(c); select only extra indices
    for i_idx_out=0l, n_idx_out-1l do begin
        idx_flag_tmp = where(idx_tmp eq idx_out[i_idx_out], n_idx_flag_tmp)
        if (temporary(n_idx_flag_tmp) gt 0l) then flag_tmp[idx_flag_tmp] = 0
    endfor
    idx_flag_tmp = where((flag_tmp eq 1) and ((c_func_flag[idx_tmp] eq 0) or (c_func_flag[idx_tmp] eq 1)), n_idx_flag_tmp) ;(c); checking excluded points (excl_flag = 2)

    ;(c); checking dublicate indices
    if (n_idx_flag_tmp gt 0l) then begin
        idx_out = [idx_out, idx_tmp[idx_flag_tmp]]
        n_idx_out += n_idx_flag_tmp
    endif
endif else message, /info, "undefined/incorrect input"

return, idx_out
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-----------------------------   DISFIT__NEIGHBOR   ---------------------------
;------------------------------------------------------------------------------

function disfit__neighbor, idx_inp, n_idx=n_idx_out

compile_opt hidden
common common__disfit__qhull
common common__disfit__funcs

idx_out = -1l
n_idx_out = 0l

if (disfit__info__check(idx_inp, n_=n_idx_inp, gt_n=0l, eq_k=1, idx=q_pnt_arr)) then begin
    idx_out = idx_inp
    n_idx_out = n_idx_inp

    ;(c); checking the completeness of an array of indices
    idx_uniq = uniq(idx_out,sort(idx_out))
    if ((n_idx_arr = n_elements(idx_uniq)) eq q_pnt_arr) then return, idx_out
    idx_arr = idx_out[temporary(idx_uniq)]

    ;(c); indices from frame range
    if (disfit__info__check(c_frame_conn_depth, eq_n=1) and disfit__info__check(c_frame_range, eq_p=2, eq_q=p_pnt_arr)) then begin
        idx_frame = idx_arr
        for i_idx_arr=0l, n_idx_arr-1l do begin
            idx_pnt_tmp = lindgen((n_idx_pnt_tmp = q_pnt_arr))
            for i_pnt_arr=0l, p_pnt_arr-1l do begin
                idx_tmp = where( $
                    (c_pnt_arr[i_pnt_arr,idx_pnt_tmp] ge c_pnt_arr[i_pnt_arr,idx_arr[i_idx_arr]] + c_frame_range[0,i_pnt_arr]) and $
                    (c_pnt_arr[i_pnt_arr,idx_pnt_tmp] le c_pnt_arr[i_pnt_arr,idx_arr[i_idx_arr]] + c_frame_range[1,i_pnt_arr]) $
                    , n_idx_pnt_tmp)
                if (n_idx_pnt_tmp gt 0l) then idx_pnt_tmp = idx_pnt_tmp[temporary(idx_tmp)] else break
            endfor
            if (n_idx_pnt_tmp gt 0l) then idx_frame = idx_frame[uniq(idx_frame, sort((idx_frame = [idx_frame, temporary(idx_pnt_tmp)])))]
        endfor

        flag_tmp = intarr((n_idx_frame = n_elements(idx_frame))) + 1
        for i_idx_arr=0l, n_idx_arr-1l do begin
            idx_flag_tmp = where(idx_frame eq idx_arr[i_idx_arr], n_idx_flag_tmp)
            if (temporary(n_idx_flag_tmp) gt 0l) then flag_tmp[idx_flag_tmp] = 0
        endfor
        idx_flag_tmp = where(flag_tmp eq 1, n_idx_flag_tmp)
        idx_frame = (n_idx_flag_tmp gt 0l)? [idx_arr, idx_frame[idx_flag_tmp]] : idx_arr

        i_idx_frame = 0l
        for i_frame_conn_depth=1l, c_frame_conn_depth do begin
            if (i_idx_frame lt (i_idx_tmp = temporary(n_idx_frame))) then begin
                idx_frame = disfit__neighbor__depth(temporary(idx_frame), temporary(i_idx_frame), n_idx=n_idx_frame)
                i_idx_frame = temporary(i_idx_tmp)
            endif
        endfor
    endif else n_idx_frame = 0l

    ;(c); indices from connectivity matrix
    if (c_conn_depth gt c_frame_conn_depth) then begin
        idx_conn = idx_arr
        n_idx_conn = n_idx_arr
        i_idx_conn = 0l
        for i_conn_depth=1l, c_conn_depth do begin
            if (i_idx_conn lt (i_idx_tmp = temporary(n_idx_conn))) then begin
                idx_conn = disfit__neighbor__depth(temporary(idx_conn), temporary(i_idx_conn), n_idx=n_idx_conn)
                i_idx_conn = temporary(i_idx_tmp)
            endif
        endfor
    endif else n_idx_conn = 0l

    ;(c); checking uniq extra indices from frame and conn
    if ((n_idx_conn gt n_idx_arr) and (n_idx_frame gt n_idx_arr)) then begin
        idx_tmp = [idx_conn[n_idx_arr:*], idx_frame[n_idx_arr:*]]
        idx_out = [idx_out, idx_tmp[uniq(idx_tmp,sort(idx_tmp))]]
    endif else begin
        if (n_idx_conn gt n_idx_arr) then idx_out = [idx_out, idx_conn[n_idx_arr:*]]
        if (n_idx_frame gt n_idx_arr) then idx_out = [idx_out, idx_frame[n_idx_arr:*]]
    endelse
    n_idx_out = n_elements(idx_out)
endif

return, idx_out
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-----------------------------   DISFIT__SIMPLEX   ----------------------------
;------------------------------------------------------------------------------

function disfit__simplex, pnt=pnt_inp, idx=idx_inp, ref_pnt=ref_pnt, ref_idx=ref_idx, n_idx=n_idx_out, _extra=extra

compile_opt hidden
common common__disfit__qhull
common common__disfit__funcs

idx_out = -1l
n_idx_out = 0l

if (n_elements(pnt_inp) ne 0) then pnt = pnt_inp
if (n_elements(idx_inp) ne 0) then idx = 0l > idx_inp < (q_pnt_arr-1l)

key_pnt = disfit__info__check(pnt, p_=p_pnt, q_=q_pnt, eq_p=p_pnt_arr, gt_q=0l)
key_idx = disfit__info__check(idx, n_=n_idx, gt_n=0l, eq_k=1, idx=q_pnt_arr)
key_ref_pnt = disfit__info__check(ref_pnt, p_=p_ref_pnt, q_=q_ref_pnt, n_=n_ref_pnt, eq_p=p_pnt_arr, eq_q=p_pnt_arr+1l)
key_ref_idx = disfit__info__check(ref_idx, n_=n_ref_idx, eq_n=p_pnt_arr+1l, eq_k=1, idx=q_pnt_arr)
if ((n_ref_pnt gt 0l) and ~keyword_set(key_ref_pnt)) then message, "incorrect input REF_PNT"
if ((n_ref_idx gt 0l) and ~keyword_set(key_ref_idx)) then message, "incorrect input REF_IDX"
if ~(key_pnt xor key_idx) then begin
    message, /info, "undefined/incorrect input PNT/IDX"
    return, idx_out
endif

if ((q_pnt gt 0l) and (n_elements(c_tri_arr) gt 0l) and (n_elements(c_norm_arr) gt 0l)) then begin
    for i_tri_arr=0l, n_elements(c_tri_arr[0,*])-1l do begin
        if ~(keyword_set(point_inside(c_pnt_arr[*,c_tri_arr[*,i_tri_arr]], $
            total(c_pnt_arr[*,c_tri_arr[*,i_tri_arr]],2) / n_elements(c_tri_arr[*,i_tri_arr]), /noboundary))) then continue
        pnt_ins = total(c_pnt_arr[*,c_tri_arr[*,i_tri_arr]],2) / n_elements(c_tri_arr[*,i_tri_arr])
        break
    endfor
    if (n_elements(pnt_ins) gt 0l) then begin
        for i_pnt=0l, q_pnt-1l do begin
            if ~(point_inside(pnt_ins, pnt[*,i_pnt], normals=c_norm_arr)) then begin
                dis = dblarr(1l,q_pnt_arr)
                for i_pnt_arr=0l, p_pnt_arr-1l do dis += (c_pnt_arr[i_pnt_arr,*] - pnt[i_pnt_arr,i_pnt])^2d
                idx_dis = sort(dis)
                pnt[*,i_pnt] = c_pnt_arr[*,idx_dis[0]]
            endif
        endfor
    endif
endif

if (key_ref_pnt xor key_ref_idx) then begin
    pnt_tmp = (n_idx gt 0l)? c_pnt_arr[*,idx] : pnt
    s_pnt_tmp = disfit__info(pnt_tmp, p_=p_pnt_tmp, q_=q_pnt_tmp)

    if (n_elements(c_norm_arr) gt 0l) then begin
        i_tri_arr = 0l
        repeat begin
            pnt_ins = total(c_pnt_arr[*,c_tri_arr[*,i_tri_arr]],2) / n_elements(c_tri_arr[*,i_tri_arr])
            i_tri_arr++
        endrep until (point_inside(pnt_ins, pnt_ins, normals=c_norm_arr, /noboundary))
        for i_pnt_tmp=0l, q_pnt_tmp-1l do begin
            if ~(point_inside(pnt_ins, pnt_tmp[*,i_pnt_tmp], normals=c_norm_arr)) then begin
                dis = dblarr(1l,q_pnt_arr)
                for i_pnt_arr=0l, p_pnt_arr-1l do dis += (c_pnt_arr[i_pnt_arr,*] - pnt_tmp[i_pnt_arr,i_pnt_tmp])^2d
                idx_dis = sort(dis)
                pnt_tmp[*,i_pnt_tmp] = c_pnt_arr[*,idx_dis[0]]
            endif
        endfor
    endif

    pnt_ref = (n_ref_idx gt 0l)? c_pnt_arr[*,ref_idx] : ref_pnt
    s_pnt_ref = disfit__info(pnt_ref, p_=p_pnt_ref, q_=q_pnt_ref)
    idx_ref = (n_ref_idx gt 0l)? ref_idx : disfit__simplex(pnt=total(ref_pnt,2l,/double)/q_ref_pnt)
    s_idx_ref = disfit__info(idx_ref, n_=n_idx_ref)

    flg_tri = bytarr(n_elements(c_tri_arr[0l,*]))
    for i_idx_ref=0l, n_idx_ref-1l do flg_tri[(array_indices(c_tri_arr, where(c_tri_arr eq idx_ref[i_idx_ref])))[1l,*]] = 1
    idx_tri = where(flg_tri eq 1l, n_idx_tri)

    idx_out = lonarr(p_pnt_arr+1l,q_pnt_tmp) - 1l
    n_idx_out = (p_pnt_arr+1l)*q_pnt_tmp
    for i_pnt_tmp=0l, q_pnt_tmp-1l do begin
        if ~(keyword_set(point_inside(pnt_ref,pnt_tmp[*,i_pnt_tmp]))) then begin
            pnt_arr = [[total(pnt_ref,2l,/double)/q_pnt_ref], [pnt_tmp[*,i_pnt_tmp]]]
            while (1) do begin
                tmp = total(pnt_arr,2l,/double) / 2d
                if ~(keyword_set(point_inside(pnt_ref,tmp))) then begin
                    for i_idx_tri=0l, n_idx_tri-1l do begin
                        ;(c); check degeneracy simplex
                        if (keyword_set(point_inside(c_pnt_arr[*,c_tri_arr[*,idx_tri[i_idx_tri]]],tmp)) and $
                            keyword_set(point_inside(c_pnt_arr[*,c_tri_arr[*,idx_tri[i_idx_tri]]], $
                                total(c_pnt_arr[*,c_tri_arr[*,idx_tri[i_idx_tri]]],2) / n_elements(c_tri_arr[*,idx_tri[i_idx_tri]]),/noboundary))) then break
                    endfor
                    if (i_idx_tri lt n_idx_tri) then begin
                        tmp = !values.d_nan
                        idx_out[*,i_pnt_tmp] = c_tri_arr[*,idx_tri[i_idx_tri]]
                        break
                    endif else pnt_arr[*,1l] = temporary(tmp)
                endif else pnt_arr[*,0l] = temporary(tmp)
                if (n_elements(where(abs(pnt_arr[*,1]-pnt_arr[*,0]) le 1d-6*total(pnt_arr,2l,/double)/2d)) eq p_pnt_arr) then begin
                    idx_out[*,i_pnt_tmp] = idx_ref
                    break
                endif
            endwhile
        endif else idx_out[*,i_pnt_tmp] = (n_ref_idx gt 0l)? ref_idx : disfit__simplex(pnt=pnt_tmp[*,i_pnt_tmp])
    endfor
endif else if (key_pnt xor key_idx) then begin
    if (key_idx) then begin
        idx_out = lonarr(p_pnt_arr+1l,n_idx) - 1l
        n_idx_out = (p_pnt_arr+1l)*n_idx
        for i_idx=0l, n_idx-1l do begin
            idx_tri = (array_indices(c_tri_arr, where(c_tri_arr eq idx[i_idx], n_idx_tri)))[1l,*]
            val = dblarr(1l,n_idx_tri,/nozero) + !values.f_nan
            for i_idx_tri=0l, n_idx_tri-1l do begin
                ;(c); check degeneracy simplex
                if ~(keyword_set(point_inside(c_pnt_arr[*,c_tri_arr[*,idx_tri[i_idx_tri]]], $
                    total(c_pnt_arr[*,c_tri_arr[*,idx_tri[i_idx_tri]]],2) / n_elements(c_tri_arr[*,idx_tri[i_idx_tri]]), /noboundary))) then continue
                val[0l,i_idx_tri] = (array_equal(c_func_flag[0l,c_tri_arr[*,idx_tri[i_idx_tri]]],intarr(p_pnt_arr+1l)+1))? total(c_func_val[0l,c_tri_arr[*,idx_tri[i_idx_tri]]],/double) : !values.d_nan
            endfor
            tmp = min(val,idx_tmp,/nan)
            idx_out[*,i_idx] = c_tri_arr[*,idx_tri[idx_tmp]]
        endfor
    endif
    if (key_pnt) then begin
        idx_out = lonarr(p_pnt_arr+1l,q_pnt) - 1l
        n_idx_out = (p_pnt_arr+1l)*q_pnt
        for i_pnt=0l, q_pnt-1l do begin
            dis = dblarr(1l,q_pnt_arr)
            for i_pnt_arr=0l, p_pnt_arr-1l do dis += (c_pnt_arr[i_pnt_arr,*] - pnt[i_pnt_arr,i_pnt])^2d
            idx_dis = sort(dis)
            idx_tmp = -1l
            for i_dis=0l, q_pnt_arr-1l do begin
                idx_tri = (array_indices(c_tri_arr, where(c_tri_arr eq idx_dis[i_dis], n_idx_tri)))[1l,*]
                for i_idx_tri=0l, n_idx_tri-1l do begin
                    ;(c); check degeneracy simplex
                    if ~(keyword_set(point_inside(c_pnt_arr[*,c_tri_arr[*,idx_tri[i_idx_tri]]], $
                        total(c_pnt_arr[*,c_tri_arr[*,idx_tri[i_idx_tri]]],2) / n_elements(c_tri_arr[*,idx_tri[i_idx_tri]]), /noboundary))) then continue
                    ;(c); check inner point of simplex
                    if (keyword_set(point_inside(c_pnt_arr[*,c_tri_arr[*,idx_tri[i_idx_tri]]], pnt[*,i_pnt], boundary=boundary))) then begin
                        if (keyword_set(boundary)) then begin
                            if (array_equal(c_func_flag[0l,c_tri_arr[*,idx_tri[i_idx_tri]]],intarr(p_pnt_arr+1l)+1)) then begin
                                idx_tmp = $
                                    (idx_tmp eq -1l)? i_idx_tri : $
                                    (total(c_func_val[0l,c_tri_arr[*,idx_tri[i_idx_tri]]],/double) lt total(c_func_val[0l,c_tri_arr[*,idx_tri[idx_tmp]]],/double))? i_idx_tri : idx_tmp
                            endif else if (total(c_func_flag[0l,c_tri_arr[*,idx_tri[i_idx_tri]]],/int) eq 0l) then begin
                                idx_tmp = i_idx_tri
                                break
                            endif
                        endif else break
                    endif
                endfor
                if (i_idx_tri lt n_idx_tri) then begin
                    idx_out[*,i_pnt] = c_tri_arr[*,idx_tri[i_idx_tri]]
                    break
                endif else if (idx_tmp ne -1l) then idx_out[*,i_pnt] = c_tri_arr[*,idx_tri[idx_tmp]]
            endfor
        endfor
    endif
endif else message, /info, "undefined/incorrect input PNT/IDX"

if (n_idx_out gt 0l) then begin
    s_idx_out = disfit__info(idx_out, p_=p_idx_out, q_=q_idx_out)
    for i_idx_out=0l, q_idx_out-1l do idx_out[*,i_idx_out] = idx_out[sort(idx_out[*,i_idx_out]),i_idx_out]
endif

return, idx_out
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;------------------------------   DISFIT__EXCLUDE   ---------------------------
;------------------------------------------------------------------------------

function disfit__exclude, idx_inp, n_idx=n_idx_out, exclidx=exclidx, exclval=exclval, exclgt=exclgt, excllt=excllt, exclnan=exclnan, exclinf=exclinf

compile_opt hidden
common common__disfit__qhull
common common__disfit__funcs

idx_out = -1l
n_idx_out = 0l
excl_flag = 2

if (disfit__info__check(idx_inp, n_=n_idx_inp, gt_n=0l, eq_k=1, idx=q_pnt_arr)) then begin
    idx_out = idx_inp
    n_idx_out = n_idx_inp
    if (disfit__info__check(exclidx, n_=n_exclidx, gt_n=0l)) then begin
        for i_exclidx=0l, n_exclidx-1l do begin
            idx_tmp = where(idx_out eq exclidx[i_exclidx], n_idx_tmp)
            if (temporary(n_idx_tmp) gt 0l) then c_func_flag[0l,idx_out[temporary(idx_tmp)]] = excl_flag
        endfor
    endif
    if (disfit__info__check(exclval, n_=n_exclval, gt_n=0l)) then begin
        for i_exclval=0l, n_exclval-1l do begin
            idx_tmp = where(c_func_val[0l,idx_out] eq exclval[i_exclval], n_idx_tmp)
            if (temporary(n_idx_tmp) gt 0l) then c_func_flag[0l,idx_out[temporary(idx_tmp)]] = excl_flag
        endfor
    endif
    if (disfit__info__check(exclgt, n_=n_exclgt, gt_n=0l)) then begin
        idx_tmp = where(c_func_val[0l,idx_out] gt max(exclgt,/nan), n_idx_tmp)
        if (temporary(n_idx_tmp) gt 0l) then c_func_flag[0l,idx_out[temporary(idx_tmp)]] = excl_flag
    endif
    if (disfit__info__check(excllt, n_=n_excllt, gt_n=0l)) then begin
        idx_tmp = where(c_func_val[0l,idx_out] lt min(excllt,/nan), n_idx_tmp)
        if (temporary(n_idx_tmp) gt 0l) then c_func_flag[0l,idx_out[temporary(idx_tmp)]] = excl_flag
    endif
    if (keyword_set(exclnan)) then begin
        idx_tmp = where(finite(c_func_val[0l,idx_out],/nan), n_idx_tmp)
        if (temporary(n_idx_tmp) gt 0l) then c_func_flag[0l,idx_out[temporary(idx_tmp)]] = excl_flag
    endif
    if (keyword_set(exclinf)) then begin
        idx_tmp = where(finite(c_func_val[0l,idx_out],/inf), n_idx_tmp)
        if (temporary(n_idx_tmp) gt 0l) then c_func_flag[0l,idx_out[temporary(idx_tmp)]] = excl_flag
    endif
    idx_tmp = where(c_func_flag[0l,idx_out] ne excl_flag, n_idx_out)
    idx_out = (n_idx_out gt 0l)? idx_out[temporary(idx_tmp)] : -1l
endif

return, idx_out
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;------------------------------   DISFIT__INCLUDE   ---------------------------
;------------------------------------------------------------------------------

function disfit__include, idx_inp, n_idx=n_idx_out, include=include, checkidx=checkidx, checkval=checkval, checkgt=checkgt, checklt=checklt, checknan=checknan, checkinf=checkinf

compile_opt hidden
common common__disfit__qhull
common common__disfit__funcs

idx_out = -1l
n_idx_out = 0l
excl_flag = 2

if (disfit__info__check(idx_inp, n_=n_idx_inp, gt_n=0l, eq_k=1, idx=q_pnt_arr)) then begin
    idx_out = idx_inp
    n_idx_out = n_idx_inp
    flag_out = intarr(n_idx_out)
    if (disfit__info__check(checkidx, n_=n_checkidx, gt_n=0l)) then begin
        for i_checkidx=0l, n_checkidx-1l do begin
            idx_tmp = where(idx_out eq checkidx[i_checkidx], n_idx_tmp)
            if (temporary(n_idx_tmp) gt 0l) then flag_out[temporary(idx_tmp)] = 1
        endfor
    endif
    if (disfit__info__check(checkval, n_=n_checkval, gt_n=0l)) then begin
        for i_checkval=0l, n_checkval-1l do begin
            idx_tmp = where(c_func_val[0l,idx_out] eq checkval[i_checkval], n_idx_tmp)
            if (temporary(n_idx_tmp) gt 0l) then flag_out[temporary(idx_tmp)] = 1
        endfor
    endif
    if (disfit__info__check(checkgt, n_=n_checkgt, gt_n=0l)) then begin
        idx_tmp = where(c_func_val[0l,idx_out] gt max(checkgt,/nan), n_idx_tmp)
        if (temporary(n_idx_tmp) gt 0l) then flag_out[temporary(idx_tmp)] = 1
    endif
    if (disfit__info__check(checklt, n_=n_checklt, gt_n=0l)) then begin
        idx_tmp = where(c_func_val[0l,idx_out] lt min(checklt,/nan), n_idx_tmp)
        if (temporary(n_idx_tmp) gt 0l) then flag_out[temporary(idx_tmp)] = 1
    endif
    if (keyword_set(checknan)) then begin
        idx_tmp = where(finite(c_func_val[0l,idx_out],/nan), n_idx_tmp)
        if (temporary(n_idx_tmp) gt 0l) then flag_out[temporary(idx_tmp)] = 1
    endif
    if (keyword_set(checkinf)) then begin
        idx_tmp = where(finite(c_func_val[0l,idx_out],/inf), n_idx_tmp)
        if (temporary(n_idx_tmp) gt 0l) then flag_out[temporary(idx_tmp)] = 1
    endif
    if (total(flag_out,/int) gt 0l) then begin
        idx_flag_out = disfit__neighbor(idx_out[where(temporary(flag_out) eq 1)], n_idx=n_idx_flag_out)
        flag_out = intarr(n_idx_flag_out) + 1
        for i_idx_out=0l, n_idx_out-1l do begin
            idx_flag = where(idx_flag_out eq idx_out[i_idx_out], n_idx_flag)
            if (n_idx_flag gt 0l) then flag_out[idx_flag] = 0
        endfor
        if ((n_idx_out = (total(flag_out,/int)+n_idx_inp)) gt n_idx_inp) then idx_out = [idx_inp, idx_flag_out[where(flag_out eq 1)]]
    endif
endif

if (keyword_set(include)) then begin
    idx_out = (n_idx_out gt n_idx_inp)? idx_out[n_idx_inp:*] : -1l
    n_idx_out = (n_idx_out - n_idx_inp) > 0l
endif

return, idx_out
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;------------------------------   DISFIT__IDXMIN   ----------------------------
;------------------------------------------------------------------------------

function disfit__idxmin, idx_arr, idx_inp, global=global, quick=quick, n_idx=n_idx_out

compile_opt hidden
common common__disfit__qhull
common common__disfit__funcs

idx_out = -1l
n_idx_out = 0l

if (keyword_set(global)) then begin
    if (disfit__info__check(c_idx_min, eq_n=0l, eq_k=1, idx=q_pnt_arr)) then begin
        idx_out = c_idx_min
        n_idx_out = 1l
    endif
    return, idx_out
endif

if (disfit__info__check(idx_arr, n_=n_idx_arr, gt_n=0l, eq_k=1, idx=q_pnt_arr)) then begin
    idx_out = idx_arr[uniq(idx_arr,sort(idx_arr))]
    n_idx_out = n_elements(idx_out)
    if (disfit__info__check(idx_inp, n_=n_idx_inp, gt_n=0l, eq_k=1, idx=q_pnt_arr) and ~keyword_set(quick)) then begin
        flag_out = intarr(n_idx_out)
        for i_idx_inp=0l, n_idx_inp-1l do begin
            idx_min = disfit__neighbor(idx_inp[i_idx_inp], n_idx=n_idx_tmp)
            idx_tmp = where(c_func_val[0l,idx_min] lt c_func_val[0l,idx_inp[i_idx_inp]], n_idx_min)
            idx_min = (n_idx_min gt 0l)? idx_min[temporary(idx_tmp)] : idx_inp[i_idx_inp]
            for i_idx_min=0l, ((n_idx_min gt 0l)? n_idx_min-1l : 0l) do flag_out[where(idx_out eq idx_min[i_idx_min])] = 1
        endfor
        if (total(flag_out,/int) gt 0l) then begin
            idx_out = idx_out[where(flag_out eq 1, n_idx_out)]
            idx_out = idx_out[uniq(idx_out,sort(idx_out))]
            n_idx_out = n_elements(idx_out)
        endif else begin
            idx_out = -1l
            n_idx_out = 0l
        endelse
    endif else begin
        tmp = min(c_func_val[0l,idx_out], i_idx_out)
        idx_out = idx_out[i_idx_out]
        n_idx_out = 1l
    endelse
endif

return, idx_out
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-----------------------------   DISFIT__IDXCHECK   ---------------------------
;------------------------------------------------------------------------------

function disfit__idxcheck, flag_arr, idx_arr, idx_inp, quick=quick, flag=flag_out, idx=idx_out, n_idx=n_idx_out

compile_opt hidden
common common__disfit__qhull
common common__disfit__funcs

key = 0
flag_out = -1
idx_out = -1l
n_idx_out = 0l

if (disfit__info__check(flag_arr, n_=n_flag_arr, gt_n=0l) and disfit__info__check(idx_arr, n_=n_idx_arr, eq_n=n_flag_arr, eq_k=1, idx=q_pnt_arr)) then begin
    if (disfit__info__check(idx_inp, n_=n_idx_inp, gt_n=0l, eq_k=1, idx=q_pnt_arr)) then begin
        idx_flag = where(flag_arr eq 0, n_idx_flag)
        idx_out = (n_idx_flag gt 0l)? [idx_inp, idx_arr[idx_flag]] : [idx_inp]
        flag_out = (n_idx_flag gt 0l)? [intarr(n_idx_inp) + 1, intarr(n_idx_flag)] : [intarr(n_idx_inp) + 1]
        idx_uniq = uniq(idx_out,sort(idx_out))
        idx_out = idx_out[idx_uniq]
        flag_out = flag_out[idx_uniq]
        n_idx_out = n_elements(idx_out)
        key = ~array_equal(idx_arr,idx_out)
        if (keyword_set(key) eq 1) then key = (min(c_func_val[idx_arr]) gt min(c_func_val[idx_out]))
    endif else begin
        flag_out = flag_arr
        idx_out = idx_arr
        n_idx_out = n_idx_arr
    endelse
endif

return, key
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-------------------------------   DISFIT__FUNCS   ----------------------------
;------------------------------------------------------------------------------

function disfit__funcs, idx_inp, parinfo=parinfo, idx_dis=idx_dis, idx_out=idx_out, n_idx_out=n_idx_out, $
    initialize=initialize, clear=clear, conn_depth=conn_depth, $
    func_name=func_name, func_prep_read=func_prep_read, func_prep_write=func_prep_write, $
    frame_conn_depth=frame_conn_depth, frame_range=frame_range, minrange=minrange, maxrange=maxrange, $
    debug=debug, _extra=extra

compile_opt hidden
common common__disfit__qhull
common common__disfit__funcs

status_funcs = 0
idx_out = -1l
n_idx_out = 0l

if (keyword_set(clear) or keyword_set(initialize)) then begin
    if (keyword_set(func_prep_write) and ~keyword_set(initialize)) then begin
        if (file_test(func_prep_write)) then file_delete, func_prep_write
        writefits, func_prep_write, /silent, c_func_val
        if (n_elements(c_parinfo) eq 1) then if (c_parinfo gt 0l) then writefits, func_prep_write, /silent, /append, c_par_val
        if (n_elements(c_parinfo) eq 1) then if (c_parinfo gt 0l) then writefits, func_prep_write, /silent, /append, c_par_err
    endif
    tmp = disfit__delete(c_func_name)
    tmp = disfit__delete(c_conn_depth)
    tmp = disfit__delete(c_frane_conn_depth)
    tmp = disfit__delete(c_frame_range)
    tmp = disfit__delete(c_minrange)
    tmp = disfit__delete(c_maxrange)
    tmp = disfit__delete(c_parinfo)
    tmp = disfit__delete(c_idx_dis)
    tmp = disfit__delete(c_ncalls)
    tmp = disfit__delete(c_idx_min)
    tmp = disfit__delete(c_func_val)
    tmp = disfit__delete(c_func_flag)
    tmp = disfit__delete(c_par_val)
    tmp = disfit__delete(c_par_err)
    if (keyword_set(clear)) then return, status_funcs
endif

if (keyword_set(initialize)) then begin
    if (status_funcs eq 0) then if (0 eq (status_funcs = ~disfit__info__check(func_name, eq_t=7, eq_n=1))) then c_func_name = func_name
    if (status_funcs eq 0) then begin
        c_ncalls = 0l
        c_conn_depth = (disfit__info__check(conn_depth, eq_n=1))? conn_depth : 1
        if (disfit__info__check(minrange, eq_d=2, eq_p=2, eq_q=p_pnt_arr)) then c_minrange = minrange
        if (disfit__info__check(maxrange, eq_2=2, eq_p=2, eq_q=p_pnt_arr)) then c_maxrange = maxrange
        c_frame_conn_depth = (disfit__info__check(frame_conn_depth, eq_n=1))? frame_conn_depth : 0
        if (disfit__info__check(frame_range, eq_d=2, eq_p=2, eq_q=p_pnt_arr)) then c_frame_range = frame_range
        c_parinfo = (disfit__info__check(parinfo, n_=n_parinfo, gt_p=0l, eq_q=1l))? n_parinfo : 0l
        c_idx_dis = (disfit__info__check(idx_dis, n_=n_idx_dis, gt_p=0l, lt_p=n_parinfo))? idx_dis : -1l
        if (keyword_set(func_prep_read)) then begin
            if (file_test(func_prep_read)) then begin
                fits_open, func_prep_read, fcn, /no_abort
                n_ext = fcn.nextend
                fits_close, temporary(fcn), /no_abort
                c_func_val = readfits(func_prep_read, ext=0, /silent)
                if (n_ext ge 1) then c_par_val = readfits(func_prep_read, ext=1, /silent) else if (c_parinfo gt 0l) then c_par_val = dblarr(c_parinfo, q_pnt_arr) + !values.d_nan
                if (n_ext ge 2) then c_par_err = readfits(func_prep_read, ext=2, /silent) else if (c_parinfo gt 0l) then c_par_err = dblarr(c_parinfo, q_pnt_arr) + !values.d_nan
                c_parinfo = (disfit__info__check(c_par_val, p_=n_parinfo, gt_p=0l, gt_q=0l))? n_parinfo : 0l
            endif else message, /info, "cache common funcs file not found"
        endif else begin
            c_func_val = (disfit__info__check(func_init, eq_d=2, eq_p=1, eq_q=q_pnt_arr))? func_init : dblarr(1,q_pnt_arr) + !values.d_nan
            if (c_parinfo gt 0l) then begin
                c_par_val = dblarr(c_parinfo, q_pnt_arr) + !values.d_nan
                c_par_err = dblarr(c_parinfo, q_pnt_arr) + !values.d_nan
            endif
        endelse
        c_func_flag = finite(c_func_val)
    endif else message, /info, "undefined/incorrect input FUNC_NAME"
    return, status_funcs
endif

if (disfit__info__check(idx_inp, n_=n_idx_inp, gt_n=0l, eq_k=1, idx=q_pnt_arr)) then begin
    idx_out = idx_inp
    n_idx_out = n_idx_inp
    ncalls = c_ncalls

    for i_idx_out=0l, n_idx_out-1l do begin
        if (c_func_flag[0l,idx_out[i_idx_out]] eq 0l) then begin
            time_func = systime(/sec)

            c_func_val[0l,idx_out[i_idx_out]] = (c_parinfo gt 0l)? $
                call_function(c_func_name, c_pnt_arr[*,idx_out[i_idx_out]], _extra=extra, parinfo=parinfo, idx_dis=c_idx_dis, pvalue=pvalue, perror=perror) : $
                call_function(c_func_name, c_pnt_arr[*,idx_out[i_idx_out]], _extra=extra)

            c_ncalls++
            c_func_flag[0l,idx_out[i_idx_out]] = 1
            if (disfit__info__check(pvalue, gt_n=0l, eq_p=c_parinfo, eq_q=1, eq_k=1, /finite)) then c_par_val[*,idx_out[i_idx_out]] = temporary(pvalue)
            if (disfit__info__check(perror, gt_n=0l, eq_p=c_parinfo, eq_q=1, eq_k=1, /finite)) then c_par_err[*,idx_out[i_idx_out]] = temporary(perror)

            if (keyword_set(debug)) then $
                print, i_idx_out+1l, n_idx_out, idx_out[i_idx_out], systime(/sec)-time_func, c_func_val[0l,idx_out[i_idx_out]], c_pnt_arr[*,idx_out[i_idx_out]], $
                    format='(i,tr1,"/",tr1,i,tr1,"|",tr1,i,tr1,"|",tr1,f,tr1,"|",tr1,f,tr1,"||",'+strtrim(p_pnt_arr,2)+'(tr1,f))'
        endif
    endfor

    idx_out = disfit__exclude(temporary(idx_out), n_idx=n_idx_out, _extra=extra)
    idx_out = disfit__include(temporary(idx_out), n_idx=n_idx_out, _extra=extra)
    if (~array_equal(idx_inp,idx_out)) then status_funcs = disfit__funcs(temporary(idx_out), idx_out=idx_out, n_idx_out=n_idx_out, parinfo=parinfo, idx_dis=idx_dis, debug=debug)
    status_funcs = (ncalls ne c_ncalls)
endif

return, status_funcs
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-----------------------------   DISFIT__TRANSFORM   --------------------------
;------------------------------------------------------------------------------

function disfit__transform, arr_inp, min=min, max=max, inverse=inverse

compile_opt hidden

if ~(disfit__info__check(arr_inp, p_=p_arr_inp, q_=q_arr_inp, gt_p=0l, gt_q=0l, eq_k=1, /finite)) then begin
    message, /info, "incorrect/undefined input ARR"
    return, !values.d_nan
endif

arr_out = arr_inp
if (disfit__info__check(min, eq_p=p_arr_inp, eq_q=1, eq_k=1, /finite) and disfit__info__check(max, eq_p=p_arr_inp, eq_q=1, eq_k=1, /finite)) then begin
    if (keyword_set(inverse)) then begin
        for i_arr_out=0l, p_arr_inp-1l do arr_out[i_arr_out,*] = arr_out[i_arr_out,*] * (max[i_arr_out] - min[i_arr_out]) / 2d0 + (max[i_arr_out] + min[i_arr_out]) / 2d0
    endif else begin
        for i_arr_out=0l, p_arr_inp-1l do arr_out[i_arr_out,*] = (arr_out[i_arr_out,*] - (max[i_arr_out] + min[i_arr_out]) / 2d0) / (max[i_arr_out] - min[i_arr_out]) * 2d0
    endelse
endif

return, arr_out
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;------------------------------   DISFIT__LOOPING   ---------------------------
;------------------------------------------------------------------------------

function disfit__looping, idx_ngb_inp, idx_src_inp, $
    idx_ngb=idx_ngb, n_idx_ngb=n_idx_ngb, idx_src=idx_src, n_idx_src=n_idx_src, $
    clear=clear

compile_opt hidden
common common__disfit__qhull
common common__disfit__funcs
common common__disfit__loop, n_idx_loop, i_idx_loop, idx_loop

looping = 0
if (keyword_set(clear)) then begin
    tmp = disfit__delete(n_idx_loop)
    tmp = disfit__delete(i_idx_loop)
    tmp = disfit__delete(idx_loop)
    return, 0
endif

key_idx_ngb_inp = disfit__info__check(idx_ngb_inp, n_=n_idx_ngb_inp, gt_n=p_pnt_arr, eq_k=1, idx=q_pnt_arr)
if (keyword_set(key_idx_ngb_inp)) then begin
    n_idx_ngb = n_idx_ngb_inp
    idx_ngb = idx_ngb_inp
endif else message, "incorrect input IDX_NGB"

key_idx_src_inp = disfit__info__check(idx_src_inp, n_=n_idx_src_inp, gt_n=p_pnt_arr, le_n=n_idx_ngb_inp, eq_k=1, idx=q_pnt_arr)
if (keyword_set(key_idx_src_inp)) then begin
    n_idx_src = n_idx_src_inp
    idx_src = idx_src_inp
endif else message, "incorrect input IDX_SRC"

num_simplex = n_idx_src - p_pnt_arr
if ~(num_simplex gt 0l) then message, "incorrect simplex number"
;------------------------------------------------------------------------------
; COMMON ARRAYS (LOOPING):

d_idx_loop = 100l
if ~(disfit__info__check(n_idx_loop, eq_n=1) and disfit__info__check(i_idx_loop, eq_n=1) and disfit__info__check(idx_loop, eq_p=p_pnt_arr+1l, gt_q=0l, eq_q=n_idx_loop)) then begin
    n_idx_loop = d_idx_loop
    i_idx_loop = 0l
    idx_loop = lonarr(p_pnt_arr+1l, d_idx_loop) - 1l
endif
while (1) do begin
    if (i_idx_loop + num_simplex ge n_idx_loop) then begin
        n_idx_loop += d_idx_loop
        idx_loop = [[idx_loop], [lonarr(p_pnt_arr+1l, d_idx_loop) - 1l]]
    endif else break
endwhile

for i=i_idx_loop, i_idx_loop+num_simplex-1l do idx_loop[*,i] = (i eq  i_idx_loop)? idx_src[0:p_pnt_arr] : idx_src[p_pnt_arr + i - i_idx_loop]

for i=i_idx_loop-1l, 0l, -1l do if (array_equal(idx_loop[*,i:i+(num_simplex-1l)],idx_loop[*,i_idx_loop:i_idx_loop+(num_simplex-1l)])) then break
if (i ge 0l) then begin
    idx_tmp = (idx_loop[*,i:i_idx_loop+(num_simplex-1l)])[*]
    idx_tmp = idx_tmp[uniq(idx_tmp,sort(idx_tmp))]
    n_idx_tmp = n_elements(idx_tmp)
    if ~(array_equal(idx_src,idx_tmp)) then begin
        ;(c); added only one new idx (minimum chisqr, and save order)
        flag_tmp = intarr(n_idx_tmp) + 1
        tmp_arr = [idx_src, idx_tmp]
        idx_sort_tmp_arr = sort(tmp_arr)
        idx_where_tmp_arr = where((tmp_arr[idx_sort_tmp_arr] eq tmp_arr[idx_sort_tmp_arr[1l:*]]) and (idx_sort_tmp_arr lt n_idx_src) and (idx_sort_tmp_arr[1l:*] ge n_idx_src), n_idx_where_tmp_arr)
        if (n_idx_where_tmp_arr gt 0l) then flag_tmp[idx_sort_tmp_arr[idx_where_tmp_arr+1l]-n_idx_src] = 0
        idx_flag_tmp = where(flag_tmp eq 1, n_idx_flag_tmp)
        if (n_idx_flag_tmp gt 0l) then begin
            min_tmp = min(c_func_val[idx_tmp[idx_flag_tmp]], i_min_idx_flag_tmp, /nan)
            idx_src = [idx_src, idx_tmp[idx_flag_tmp[i_min_idx_flag_tmp]]]
            n_idx_src++
            if (n_elements(idx_src) ne n_elements(uniq(idx_src,sort(idx_src)))) then message, "incorrect indices"
        endif else looping = 1
        idx_ngb = disfit__neighbor(idx_src, n_idx=n_idx_ngb)
    endif else looping = 1
endif
i_idx_loop += num_simplex
;______________________________________________________________________________
return, looping 
end
;______________________________________________________________________________

;------------------------------------------------------------------------------
;-----------------------------   DISFIT__QUADRATIC   --------------------------
;------------------------------------------------------------------------------

function disfit__quadratic, idx_ngb_inp, idx_src_inp, $
    idx_ngb=idx_ngb, n_idx_ngb=n_idx_ngb, idx_src=idx_src, n_idx_src=n_idx_src, $
    pos_min=pos_min, pos_val=pos_val, pos_par_val=pos_par_val, pos_par_err=pos_par_err, pos_weights=pos_weights, $
    clear=clear, debug=debug

compile_opt hidden
common common__disfit__qhull
common common__disfit__funcs

quadratic = 0
idx_ngb = (idx_src = -1l)
n_idx_ngb = (n_idx_src = 0l)
tmp = disfit__delete(pos_weights)
tmp = disfit__delete(pos_min)
tmp = disfit__delete(pos_val)
tmp = disfit__delete(pos_par_val)
tmp = disfit__delete(pos_par_err)

if (disfit__info__check(idx_ngb_inp, n_=n_idx_ngb_inp, gt_n=0l, eq_k=1, idx=q_pnt_arr) and $
    disfit__info__check(idx_src_inp, n_=n_idx_src_inp, gt_n=0l, le_n=n_idx_ngb_inp, eq_k=1, idx=q_pnt_arr)) then begin

    sylvester = 0
    minimum = 0
    simplex = 0
    looping = disfit__looping(idx_ngb_inp, idx_src_inp, idx_ngb=idx_ngb, n_idx_ngb=n_idx_ngb, idx_src=idx_src, n_idx_src=n_idx_src)
    for i_idx_src=0l, n_idx_src-1l do if (idx_src[i_idx_src] ne idx_ngb[i_idx_src]) then break
    if (i_idx_src eq n_idx_src) then idx = lindgen(n_idx_src) else message, /info, "incorrect/undefined IDX"
    idx_slx = idx[0:p_pnt_arr]
    n_idx_slx = p_pnt_arr+1l

    xarr = c_pnt_arr[*,idx_ngb]
    xarr_min = (xarr_max = dblarr(p_pnt_arr) + !values.d_nan)
    for i_pnt_arr=0l, p_pnt_arr-1l do begin
        tmp_min = min(xarr[i_pnt_arr,*], max=tmp_max)
        xarr_min[i_pnt_arr] = temporary(tmp_min)
        xarr_max[i_pnt_arr] = temporary(tmp_max)
    endfor

    yarr = c_func_val[0l,idx_ngb]
    yarr_min = min(yarr, idx_yarr_min, max=yarr_max, subscript_max=idx_yarr_max, /nan)

    warr = dblarr(1, n_idx_ngb) + 1d

    xarr = disfit__transform(xarr, min=xarr_min, max=xarr_max)
    yarr = disfit__transform(yarr, min=yarr_min, max=yarr_max)

    coef = mdqf(action='coef', xarr=xarr, yarr=yarr, warr=warr, idx=idx, xdeg=(xdeg=total(xarr[*,idx_slx],2,/double)/n_idx_slx), /posdef, $
        sylvester=sylvester, xmin=xmin, exmin=exmin, ymin=ymin, eymin=eymin, weights=wght, status=status, errmsg=errmsg)

    if (keyword_set(debug)) then print, status, sylvester, point_inside(xarr[*,idx[0:p_pnt_arr]],xmin), ymin, min(yarr), format='("FUNCTION MDQF (status,sylvester,inside,ymin,min(yarr)):",tr3,3(i,tr3),tr2,2(f,tr3))'
;(-);--------------------------------------------------------------------------
    tmp_wght = wght
    ;(c); temporary weights calculation (simplex distance)
    wght[*] = 0
    tmp = dblarr(1,p_pnt_arr+1l)
    for i_pnt_arr=0l, p_pnt_arr-1l do tmp[*] += (xarr[i_pnt_arr,0l:p_pnt_arr] - xmin[i_pnt_arr])^2.
    
    if (min(tmp,i_tmp) eq 0.) then begin
        wght[i_min] = 1.
    endif else begin
        wght[0l:p_pnt_arr] = 1./tmp
        wght /= total(wght)
    endelse
;(-);__________________________________________________________________________
    if (looping eq 0) then begin
        if (array_equal(idx_src_inp,idx_src)) then begin
            if (sylvester eq 1) then begin
                xarr = disfit__transform(xarr, min=xarr_min, max=xarr_max, /inverse)
                yarr = disfit__transform(yarr, min=yarr_min, max=yarr_max, /inverse)
                xmin = disfit__transform(xmin, min=xarr_min, max=xarr_max, /inverse)
                ymin = disfit__transform(ymin, min=yarr_min, max=yarr_max, /inverse)
                if ((minimum = (ymin le min(yarr))) eq 1) then begin
                    if ~((simplex = point_inside(xarr[*,idx[0:p_pnt_arr]],xmin)) eq 1) then $
                        idx_ngb = disfit__neighbor((idx_src = disfit__simplex(pnt=xmin, ref_idx=(temporary(idx_src))[0:p_pnt_arr], n_idx=n_idx_src)), n_idx=n_idx_ngb)
                endif else message, /info, "incorrect minimum value"
            endif else message, /info, "minimum not found (sylvester criterion ne 1)"
        endif else message, /info, "simplex change"
    endif else message, /info, "looping"

    quadratic = (keyword_set(looping) or (keyword_set(minimum) and keyword_set(simplex)))

    if (keyword_set(debug)) then message, /info, "(quadratic | sylvester / minimum / simplex / looping): " + strtrim(quadratic,2) + " | " + strjoin(strtrim([sylvester,minimum,simplex,looping],2)," / ")

    if (keyword_set(quadratic)) then begin
        if ~((abs(total(wght)-1) lt 1d-7) and (max(wght) lt 1.+1d-7)) then begin
            message, /info, "incorrect weights (extrapolation?)"
            print, wght, format='("WEIGHTS:",/,'+strtrim(n_idx_ngb,2)+'f)'
        endif
        pos_weights = temporary(wght)
        pos_min = mdqf(action='value', yarr=c_pnt_arr[*,idx_ngb], parr=pos_weights)
        pos_val = mdqf(action='value', yarr=c_func_val[*,idx_ngb], parr=pos_weights)
        if (c_parinfo gt 0l) then begin
            pos_par_val = mdqf(action='value', yarr=c_par_val[*,idx_ngb], parr=pos_weights)
            pos_par_err = mdqf(action='value', yarr=c_par_err[*,idx_ngb], parr=pos_weights)
            pos_par_err[c_idx_dis] = exmin * ((xarr_max-xarr_min)/2.) / sqrt((yarr_max-yarr_min)/2.)
        endif
    endif
endif else message, /info, "undefined/incorrect input NGB/SRC"

return, quadratic
end
;______________________________________________________________________________


;------------------------------------------------------------------------------
;-----------------------------   DISFIT__INITIAL    ---------------------------
;------------------------------------------------------------------------------

function disfit__initial, idx_initial=idx_init, position=pos_init, scanning=scanning, scan_range=scan_range, n_idx=n_idx

compile_opt hidden
common common__disfit__qhull

idx = -1l
n_idx = 0l

if (keyword_set(scanning)) then return, lindgen((n_idx = q_pnt_arr))
if (disfit__info__check(scan_range, eq_p=2, eq_q=p_pnt_arr)) then begin
    idx_scan = lindgen((n_idx_scan = q_pnt_arr))
    for i_pnt_arr=0l, p_pnt_arr-1l do begin
        idx_tmp = where((c_pnt_arr[i_pnt_arr,idx_scan] ge scan_range[0l,i_pnt_arr]) and (c_pnt_arr[i_pnt_arr,idx_scan] le scan_range[1l,i_pnt_arr]), n_idx_scan)
        if (n_idx_scan gt 0l) then begin
            idx_scan = idx_scan[temporary(idx_tmp)]
        endif else break
    endfor
    if (n_idx_scan eq 0l) then idx_scan = -1l
endif else n_idx_scan = 0l
if (disfit__info__check(pos_init, eq_p=p_pnt_arr, gt_q=0l)) then idx_slx = disfit__simplex(pnt=pos_init, n_idx=n_idx_slx) else n_idx_slx = 0l
if (disfit__info__check(idx_init, n_=n_idx_pnt, gt_n=0l, eq_k=1, idx=q_pnt_arr)) then idx_pnt = idx_init else n_idx_pnt = 0l
if ((n_idx = n_idx_pnt + n_idx_slx + n_idx_scan) gt 0l) then begin
    idx = lonarr(n_idx)
    i_idx = 0l
    if (n_idx_pnt gt 0l) then begin & idx[i_idx:i_idx+n_idx_pnt-1l] = temporary(idx_pnt) & i_idx += temporary(n_idx_pnt) & endif
    if (n_idx_slx gt 0l) then begin & idx[i_idx:i_idx+n_idx_slx-1l] = temporary(idx_slx) & i_idx += temporary(n_idx_slx) & endif
    if (n_idx_scan gt 0l) then begin & idx[i_idx:i_idx+n_idx_scan-1l] = temporary(idx_scan) & i_idx += temporary(n_idx_scan) & endif
    idx = idx[uniq(idx,sort(idx))]
    n_idx = n_elements(idx)
endif else idx = disfit__simplex(pnt=total(c_pnt_arr,2,/double)/q_pnt_arr, n_idx=n_idx)

return, idx
end
;______________________________________________________________________________

;==============================================================================
;===================================   DISFIT   ===============================
;==============================================================================

function disfit, $
    pnt_arr=pnt_arr, conn_arr=conn_arr, tri_arr=tri_arr, norm_arr=norm_arr, $ ;(c); input array of points coordinates, connectivity array, traingulation array, bounds normals array
    pnt_prep_read=pnt_prep_read, pnt_prep_write=pnt_prep_write, func_prep_read=func_prep_read, func_prep_write=func_prep_write, rewrite=rewrite, cache=cache, clear=clear, $ ;(c);
    func_name=func_name, func_initial=func_init, idx_initial=idx_init, pos_initial=pos_init, $ ;(c); name fit function, initial approximation, index, coordinate
    scan=scanning, srange=scan_range, $ ;(c); scanning keyword and scan range array
    conn_depth=conn_depth, minrange=minrange, maxrange=maxrange, $ ;(c); connectivity depth (conn_depth=1), minimum and maximum ranges for connectivity
    frame_conn_depth=frame_conn_depth, frame_range=frame_range, $ ;c); frame connectivity depth (frame_conn_depth=0), frame range around solution
    quadratic=quadratic, approximation=approximation, search_initial=search_initial, quick=quick, $ ;(c);
    idx_pnt=idx_pnt, idx_src=idx_src, idx_ngb=idx_ngb, $ ;(c);
    n_idx_pnt=n_idx_pnt, n_idx_src=n_idx_src, n_idx_ngb=n_idx_ngb, n_pos_min=n_pos_min, ncalls=ncalls, $ ;(c); 
    pnt_min=pnt_min, pnt_val=pnt_val, pnt_par_val=pnt_par_val, pnt_par_err=pnt_par_err, $ ;(c); output keywords for minimal value grid point
    pos_min=pos_min, pos_val=pos_val, pos_par_val=pos_par_val, pos_par_err=pos_par_err, $ ;(c); output keywords for minimal calculated value position
    points=points, values=values, pvalues=pvalues, perrors=perrors, weights=weights, $ ;(c); output keywords for selected points
    print=print, verbose=verbose, debug=debug, silent=silent, status=status, errmsg=errmsg, _extra=extra
;(extra_keywords);    checkidx=checkidx, checkval=checkval, checkgt=checkgt, checklt=checklt, checknan=checknan, checkinf=checkinf ;(c); for __INCLUDE() points
;(extra_keywords);    exclidx=exclidx, exclval=exclval, exclgt=exclgt, excllt=excllt, exclnan=exclnan, exclinf=exclinf ;(c); for __EXCLUDE() points

;------------------------------------------------------------------------------
; INPUT, KEYWORDS and COMMON:

if (keyword_set(debug)) then time_start = (time_tmp = systime(/sec))
status = 0
errmsg = ''

idx_pnt = (idx_src = (idx_ngb = -1l))
n_idx_pnt = (n_idx_src = (n_idx_ngb = (n_pos_min = 0l)))
ncalls = 0l
if (n_elements(conn_depth) ne 1l) then conn_depth = 1
if (n_elements(frame_conn_depth) ne 1l) then frame_conn_depth = 0
if (keyword_set(scanning) and (n_elements(search_initial) eq 0l)) then search_initial = 1
if (n_elements(quadratic) ne 1l) then quadratic = 1
if (n_elements(quick) ne 1l) then quick = 1

if (keyword_set(clear)) then return, disfit__qhull(/clear)

;(c); minimum values grids points (local extremums: N_IDX_PNT)
tmp = disfit__delete(pnt_min)
tmp = disfit__delete(pnt_val)
tmp = disfit__delete(pnt_par_val)
tmp = disfit__delete(pnt_par_err)
;(c); minimum values quadratick (local extremums: N_IDX_POS)
tmp = disfit__delete(pos_min)
tmp = disfit__delete(pos_val)
tmp = disfit__delete(pos_par_val)
tmp = disfit__delete(pos_par_err)
;(c); smallest of minimum values (IDX_NGB, IDX_SRC for interpolation)
tmp = disfit__delete(points)
tmp = disfit__delete(values)
tmp = disfit__delete(weights)
tmp = disfit__delete(pvalues)
tmp = disfit__delete(perrors)

;(c); creation qhull common block
tmp = disfit__qhull(pnt_arr=pnt_arr, conn_arr=conn_arr, tri_arr=tri_arr, norm_arr=norm_arr, p_pnt_arr=p_pnt_arr, q_pnt_arr=q_pnt_arr,  $
            pnt_prep_read=pnt_prep_read, pnt_prep_write=pnt_prep_write, rewrite=rewrite, cache=cache)

if (keyword_set(debug)) then message, /info, "(TIMER:   "+strtrim(systime(/sec)-time_start,2)+"   |   "+strtrim(systime(/sec)-time_tmp,2)+"): common QHULL"
time_tmp = systime(/sec)

;(c); creation funcs common block
tmp = disfit__funcs(func_name=func_name, func_init=func_init, conn_depth=conn_depth, $
            frame_conn_depth=frame_conn_depth, frame_range=frame_range, minrange=minrange, maxrange=maxrange, /initialize, $
            func_prep_read=func_prep_read, func_prep_write=func_prep_write, rewrite=rewrite, _extra=extra)

n_parinfo = disfit__common(/parinfo)
if (keyword_set(debug)) then message, /info, "(TIMER:   "+strtrim(systime(/sec)-time_start,2)+"   |   "+strtrim(systime(/sec)-time_tmp,2)+"): common FUNCS"
time_tmp = systime(/sec)

;(c); initialize initial indices (IDX_START)
idx_pnt = disfit__initial(idx_initial=idx_init, position=pos_init, scanning=scanning, scan_range=scan_range, n_idx=n_idx_pnt)
if (keyword_set(debug)) then message, /info, "(TIMER:   "+strtrim(systime(/sec)-time_start,2)+"   |   "+strtrim(systime(/sec)-time_tmp,2)+"): initial values"
time_tmp = systime(/sec)
;______________________________________________________________________________
;------------------------------------------------------------------------------
; DISCRETE FITTING:

if (disfit__info__check(idx_pnt, gt_n=0l, eq_k=1, idx=q_pnt_arr)) then begin
    idx_funcs = (n_elements(scan_range) eq 0)? disfit__neighbor(idx_pnt, n_idx=n_idx_funcs) : idx_pnt
    n_idx_funcs = n_elements(idx_funcs)
    pnt_flag = intarr(n_idx_pnt) + 1
    ;(c); searching the minimum value of a function on a discrete grid from an initial node
    repeat begin
        key_minimum = 1
        ;(c); searching the minimum value of a function by connectivity matrix
        if (~keyword_set(approximation)) then begin
            repeat begin
                key_search = 1
                status_funcs = disfit__funcs(temporary(idx_funcs), idx_out=idx_funcs, n_idx_out=n_idx_funcs, _extra=extra, debug=debug)
                idx_min = disfit__idxmin(temporary(idx_funcs), idx_pnt, quick=quick, n_idx=n_idx_min)
                if (disfit__idxcheck(temporary(pnt_flag), temporary(idx_pnt), temporary(idx_min), quick=quick, flag=pnt_flag, idx=idx_pnt, n_idx=n_idx_pnt)) then begin
                    if (total(pnt_flag,/int) gt 0l) then begin
                        idx_funcs = disfit__neighbor(idx_pnt[where(pnt_flag eq 1)], n_idx=n_idx_funcs)
                        key_search = 0
                    endif
                endif
            endrep until (key_search)
            if (keyword_set(debug)) then message, /info, "(TIMER:   "+strtrim(systime(/sec)-time_start,2)+"   |   "+strtrim(systime(/sec)-time_tmp,2)+"): minimum value on the grid"
            time_tmp = systime(/sec)
        endif
        ;(c); searching the minimum value of a function by a quadratic form profile approximation in a simplex and adjacent nodes
        if (keyword_set(quadratic) or keyword_set(approximation)) then begin
            pos_min = dblarr(p_pnt_arr, (n_idx_pos = n_idx_pnt)) + !values.d_nan
            pos_val = dblarr(1l, n_idx_pnt) + !values.d_nan
            if (n_parinfo gt 0l) then begin
                pos_par_val = dblarr(n_parinfo, n_idx_pnt) + !values.d_nan
                pos_par_err = dblarr(n_parinfo, n_idx_pnt) + !values.d_nan
            endif
            pnt_flag_tmp = pnt_flag
            idx_pnt_tmp = idx_pnt
            for i_idx_pnt=0l, n_idx_pnt-1l do begin
                if (pnt_flag[i_idx_pnt] eq 1) then begin
                    idx_ngb_tmp = disfit__neighbor((idx_src_tmp = disfit__simplex(idx=idx_pnt[i_idx_pnt], n_idx=n_idx_src_tmp, _extra=extra)), n_idx=n_idx_ngb_tmp)
                    repeat begin
                        key_quadratic = 1
                        status_funcs = disfit__funcs(temporary(idx_ngb_tmp), idx_out=idx_ngb_tmp, n_idx_out=n_idx_ngb_tmp, _extra=extra, debug=debug)
                        idx_min_tmp = disfit__idxmin(idx_ngb_tmp, quick=quick, n_idx=n_idx_min_tmp)
                        if ((disfit__idxcheck(pnt_flag[i_idx_pnt], idx_pnt[i_idx_pnt], idx_min_tmp, quick=quick) eq 0) or (keyword_set(approximation))) then begin
                            if (disfit__quadratic(temporary(idx_ngb_tmp), temporary(idx_src_tmp), $
                                    idx_ngb=idx_ngb_tmp, n_idx_ngb=n_idx_ngb_tmp, idx_src=idx_src_tmp, n_idx_src=n_idx_src_tmp, $
                                    pos_min=pos_min_tmp, pos_val=pos_val_tmp, pos_par_val=pos_par_val_tmp, pos_par_err=pos_par_err_tmp, $
                                    pos_weights=pos_weights_tmp, debug=debug) eq 1) then pnt_flag[i_idx_pnt] = 0 else key_quadratic = 0
                        endif else begin
                            idx_pnt_tmp[where(idx_pnt_tmp eq idx_pnt[i_idx_pnt])] = idx_min_tmp[0]
                            idx_min_tmp = [idx_pnt_tmp, temporary(idx_min_tmp)]
                            tmp = disfit__idxcheck(temporary(pnt_flag_tmp), temporary(idx_pnt_tmp), temporary(idx_min_tmp), quick=quick, flag=pnt_flag_tmp, idx=idx_pnt_tmp, n_idx=n_idx_pnt_tmp)
                        endelse
                    endrep until (key_quadratic)
                    tmp = disfit__looping(/clear)
                endif
                if (pnt_flag[i_idx_pnt] eq 0) then begin
                    pos_min[*,i_idx_pnt] = temporary(pos_min_tmp)
                    pos_val[0l,i_idx_pnt] = temporary(pos_val_tmp)
                    if (n_parinfo gt 0l) then begin
                        pos_par_val[*,i_idx_pnt] = temporary(pos_par_val_tmp)
                        pos_par_err[*,i_idx_pnt] = temporary(pos_par_err_tmp)
                    endif
                    if (pos_val[0l,i_idx_pnt] eq min(pos_val,/nan)) then begin
                        idx_src = temporary(idx_src_tmp)
                        n_idx_src = temporary(n_idx_src_tmp)
                        idx_ngb = temporary(idx_ngb_tmp)
                        n_idx_ngb = temporary(n_idx_ngb_tmp)
                        weights = temporary(pos_weights_tmp)
                    endif
                endif
            endfor
            if (total(pnt_flag,/int) ne 0l) then begin
                idx_pnt = temporary(idx_pnt_tmp)
                n_idx_pnt = temporary(n_idx_tmp_pnt)
                pnt_flag = temporary(pnt_flag_tmp)
                idx_funcs = disfit__neighbor(idx_pnt, n_idx=n_idx_funcs)
                key_minimum = 0
            endif
        endif else begin
            pnt_flag[*] = 0
            idx_ngb = disfit__neighbor((idx_src = disfit__idxmin(idx_pnt, n_idx=n_idx_src)), n_idx=n_idx_ngb)
        endelse
        if (keyword_set(debug)) then message, /info, "(TIMER:   "+strtrim(systime(/sec)-time_start,2)+"   |   "+strtrim(systime(/sec)-time_tmp,2)+"): minimum grid search ITERATION"
        time_tmp = systime(/sec)
    endrep until (key_minimum)
endif else if (~keyword_set(silent)) then message, "undefined/incorrect initial indices"
if (keyword_set(debug)) then message, /info, "(TIMER:   "+strtrim(systime(/sec)-time_start,2)+"   |   "+strtrim(systime(/sec)-time_tmp,2)+"): minimum grid search COMPLITED"
time_tmp = systime(/sec)
;______________________________________________________________________________
;------------------------------------------------------------------------------
; OUTPUT:

ncalls = disfit__common(/ncalls)
;(c); minimum values grids points (local extremums: N_IDX_PNT)
if (n_idx_pnt gt 0l) then begin
    pnt_min = disfit__common(idx_pnt, /pnt_arr)
    pnt_val = disfit__common(idx_pnt, /func_val)
    if (n_parinfo gt 0l) then begin
        pnt_par_val = disfit__common(idx_pnt, /par_val)
        pnt_par_err = disfit__common(idx_pnt, /par_err)
    endif
endif
;(c); minimum values quadratick (local extremums: N_IDX_POS)
if (n_idx_pos eq 0l) then begin
    tmp = disfit__delete(pos_min)
    tmp = disfit__delete(pos_val)
    tmp = disfit__delete(pos_par_val)
    tmp = disfit__delete(pos_par_err)
endif
;(c); smallest of minimum values (IDX_NGB, IDX_SRC for interpolation)
if (n_idx_ngb gt 0l) then begin
    points = disfit__common(idx_ngb, /pnt_arr)
    values = disfit__common(idx_ngb, /func_val)
    if (n_parinfo gt 0l) then begin
        pvalues = disfit__common(idx_ngb, /par_val)
        perrors = disfit__common(idx_ngb, /par_err)
    endif
endif

if (~keyword_set(cache)) then begin
    pnt_arr = disfit__common(/pnt_arr)
    tmp = disfit__qhull(/clear)
endif
tmp = disfit__funcs(/clear, func_prep_write=func_prep_write, rewrite=rewrite)

;(c); status report
if (n_idx_pnt ne 1l) then message, /info, (errmsg = strtrim((status = 1),2)) ;(stop);;(c); mnojestvennie localnie minimumi na setke
if (n_idx_pos ne 1l) then message, /info, (errmsg = strtrim((status = 2),2)) ;(stop);;(c); mnojestvennie localnie minimumi
if (n_idx_ngb eq 0l) then message, /info, (errmsg = strtrim((status = 3),2)) ;(stop);;(c); minimum ne opredelen

if (keyword_set(debug)) then message, /info, "(TIMER:   "+strtrim(systime(/sec)-time_start,2)+"   |   "+strtrim(systime(/sec)-time_tmp,2)+"): DISFIT FUNCTION COMPLITED"
time_tmp = systime(/sec)
;______________________________________________________________________________

return, idx_pnt
end
;______________________________________________________________________________
