# Hybrid minimization algorithm

This repository contains [IDL](https://www.l3harrisgeospatial.com/Software-Technology/IDL)/[GDL](https://github.com/gnudatalanguage/gdl) code of the algorithm developed by VOxAstro team for the full spectral fitting applications.

It is designed specificly for discrete model grids and avoids the computationaly greedy interpolation of models for off-node parameter values. 

In the first step algorithm finds a minimal grid node value, then objective function is approximated with a multidimensional positive-definite quadratic form and lastly minimum position is analytically calculated using the quadratic form coefficients.


## Dependencies
* [Astrolib](https://idlastro.gsfc.nasa.gov/contents.html)
* [mpfit](https://pages.physics.wisc.edu/~craigm/idl/fitting.html)
* [coyote](https://github.com/idl-coyote/coyote) library for graphical example


## Example

Run example in the `example/` folder.
```
IDL> .run example__disfit__func.pro
```

![Example](example/fig.jpg)