;------------------------------------------------------------------------------
;+
; FILENAME: disfit__points.pro
;
; VERSION: 1.0.200812
;-
;______________________________________________________________________________

;==============================================================================
;===============================   DISFIT__POINTS   ===========================
;==============================================================================

function disfit__points, num, xrange=xrange, yrange=yrange, rrange=rrange, uniform=uniform, normal=normal, regular=regular, radial=radial

xarr = !null
if (num eq !null) then message, "undefined input NUM"
if (xrange eq !null) then xrange = [-1d,1d]
if (yrange eq !null) then yrange = [-1d,1d]
if (rrange eq !null) then rrange = [0d,1d]

if (keyword_set(regular)) then begin
    if (n_elements(num) ne 2) then message, "incorrect input NUM (num = [m,n])"
    xarr = dblarr(2l,product(num),/nozero)
    st = dindgen(num[0])
    nd = dindgen(num[1])
    for i=0l, num[0]-1l do begin
        for j=0, num[1]-1l do begin
            if (keyword_set(radial)) then begin
                xarr[0l,i*num[1]+j] = (st[i]/(num[0]-1l) * (rrange[1]-rrange[0]) + rrange[0]) * cos(nd[j]/(num[1]) * !dpi * 2d0)
                xarr[1l,i*num[1]+j] = (st[i]/(num[0]-1l) * (rrange[1]-rrange[0]) + rrange[0]) * sin(nd[j]/(num[1]) * !dpi * 2d0)
            endif else begin
                xarr[0l,i*num[1]+j] = st[i]/(num[0]-1l) * (xrange[1]-xrange[0]) + xrange[0]
                xarr[1l,i*num[1]+j] = nd[j]/(num[1]-1l) * (yrange[1]-yrange[0]) + yrange[0]
            endelse
        endfor
    endfor
endif else begin
    if (n_elements(num) ne 1l) then message, "incorrect input NUM (num = n)"
    xarr = dblarr(2l,num,/nozero)
    iarr = 0l
    while (iarr lt num) do begin
        xarr[0l,iarr] = ((keyword_set(uniform))? randomu(seed) : randomn(seed)) * (xrange[1]-xrange[0]) + xrange[0]
        xarr[1l,iarr] = ((keyword_set(uniform))? randomu(seed) : randomn(seed)) * (yrange[1]-yrange[0]) + yrange[0]
        if (keyword_set(radial)) then begin
            r = sqrt(xarr[0l,iarr]^2d0 + xarr[1l,iarr]^2d0)
            if ((r ge rrange[0]) and (r le rrange[1])) then iarr++
        endif else begin
            if ((xarr[0l,iarr] ge xrange[0]) and (xarr[0l,iarr] le xrange[1]) and (xarr[1l,iarr] ge yrange[0]) and (xarr[1l,iarr] le yrange[1])) then iarr++
        endelse
    endwhile
endelse

return, xarr
end
;______________________________________________________________________________
