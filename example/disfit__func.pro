;------------------------------------------------------------------------------
;+
; FILENAME: disfit__func.pro
;
; VERSION: 1.0.200825
;-
;______________________________________________________________________________

;==============================================================================
;================================   DISFIT__FUNC   ============================
;==============================================================================

function disfit__func, xarr, functest=func, coeff=coeff, log=log, random=random, xmin=xmin

p_xarr = n_elements(xarr[*,0])
q_xarr = n_elements(xarr[0,*])
farr = dblarr(1,q_xarr) + !values.d_nan

if (func ne !null) then begin
    if (func eq 'exp') then begin
        if (coeff eq !null) then begin
            coeff = (keyword_set(random))? (randomu(!null,6)*2d0-1d0) : [1d,0d,1d,0d,1d,1d]
            coeff[[1,3]] /= 2
            coeff[[0,2,4]] = abs(coeff[[0,2,4]])
        endif
        xmin = coeff[[1,3]]
        farr[*] = -coeff[0] * exp(-((xarr[0,*]-coeff[1])^2)/coeff[2] - (xarr[1,*]-coeff[3])^2/coeff[4]) + coeff[5]
    endif else if (func eq 'rosenbrock') then begin
        if (coeff eq !null) then coeff = [2d, 2d, 1d, 1d2]
        farr[*] = (coeff[2] - coeff[0]*xarr[0,*])^2d0 + coeff[3]*(coeff[1]*xarr[1,*] - (coeff[0]*xarr[0,*])^2d0)^2d0
    endif else if (func eq 'matyas') then begin
        if (coeff eq !null) then coeff = [1d1, 1d1, 0.26d, 0.5d] ;[0.4d, 0.78d]
        farr[*] = coeff[2]*((coeff[0]*xarr[0,*])^2d0 + (coeff[1]*xarr[1,*])^2d0) - coeff[3]*coeff[0]*xarr[0,*]*coeff[1]*xarr[1,*]
    endif else if (func eq 'sphere') then begin
        if (coeff eq !null) then coeff = [2d, 2d, 3d, 4d]
        farr[*] = coeff[2]*(coeff[0]*xarr[0,*])^2d0 + coeff[3]*(coeff[1]*xarr[1,*])^2d0
    endif else if (func eq 'mccormick') then begin
        if (coeff eq !null) then coeff = [!dpi/2d, !dpi/2d, 1.5d, 2.5d, 1d]
        farr[*] = sin(coeff[0]*xarr[0,*] + coeff[1]*xarr[1,*]) + (coeff[0]*xarr[0,*] - coeff[1]*xarr[1,*])^2d0 - coeff[2]*coeff[0]*xarr[0,*] + coeff[3]*coeff[1]*xarr[1,*] + coeff[4]
    endif else if (func eq 'easom') then begin
        if (coeff eq !null) then coeff = [!dpi/2d, !dpi/2d, !dpi, !dpi]
        farr[*] = - cos(coeff[0]*xarr[0,*]) * cos(coeff[1]*xarr[1,*]) * exp(-((coeff[0]*xarr[0,*]-coeff[2])^2d0 + (coeff[1]*xarr[1,*]-coeff[3])^2d0))
    endif else if (func eq 'sinc') then begin
        if (coeff eq !null) then coeff = [!dpi/2d, !dpi/2d]
        farr[*] = - sin(coeff[0]*xarr[0,*]^2d0 + coeff[1]*xarr[1,*]^2d0) / (coeff[0]*xarr[0,*]^2d0 + coeff[1]*xarr[1,*]^2d0)
    endif else message, "incorrect FUCN (rosenbrock/sphere/matyas/mccormick/easom)"
endif else begin
    if (coeff eq !null) then begin
        if (keyword_set(random)) then begin
            array = randomu(seed, p_xarr, p_xarr)*2d0-1d0
            array = (array ## transpose(array))
            idx_select = lindgen(p_xarr)
            for i=1l, p_xarr-1l do idx_select = [idx_select, (p_xarr+1l)*i+lindgen(p_xarr-i)]
            coeff = [(array/(identity(p_xarr)+1l))[idx_select], dblarr(p_xarr), randomu(seed,1)]
        endif else coeff = [0.27913370d, -0.86084953d, 0.66420600d, -1.9520670e-13, -7.6483264e-12, 0.099394023]
    endif
    xmin = -coeff[[3,4]]
    farr[*] = coeff ## transpose(mdqf(act='mat',xarr=xarr))
endelse

return, (keyword_set(log))? alog(farr) : farr
end
;______________________________________________________________________________
