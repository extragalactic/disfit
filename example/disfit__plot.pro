;------------------------------------------------------------------------------
;+
; FILENAME: disfit__plot.pro
;
; DESCRIPTION:
;   procedure for tests visualization
;
; INPUT:
;
;   xarr -- [m,n] array of coordinats
;   yarr -- [1,n] array of values (chisqr)
;   carr -- array of connectivity
;   idx_ngb -- 
;   idx_src --
;
;
; VERSION: 1.0.201113
;-
;______________________________________________________________________________

;==============================================================================
;==============================   DISFIT__PLOT   ==============================
;==============================================================================

pro disfit__plot, xarr=xarr, yarr=yarr, carr=carr, idx_ngb=idx_ngb, idx_src=idx_src, $
    position=position, idx_ini=idx_ini, idx_min=idx_min, cont=cont, xcont=xcont, ycont=ycont, $
    xrange=xrange, yrange=yrange, $
    logcolor=logcolor, overplot=overplot, path=path, wait=wait, stop=stop

compile_opt idl2
common common__disfit__plot, c_xarr, c_yarr, c_carr, c_cont, c_xcont, c_ycont, c_idx_ini, c_idx_min, c_path

if (xarr ne !null) then c_xarr = xarr else if (c_xarr ne !null) then xarr = c_xarr
if (yarr ne !null) then c_yarr = yarr else if (c_yarr ne !null) then yarr = c_yarr
if (carr ne !null) then c_carr = carr else if (c_carr ne !null) then carr = c_carr
if (cont ne !null) then c_cont = cont else if (c_cont ne !null) then cont = c_cont
if (xcont ne !null) then c_xcont = xcont else if (c_xcont ne !null) then xcont = c_xcont
if (ycont ne !null) then c_ycont = ycont else if (c_ycont ne !null) then ycont = c_ycont
if (idx_ini ne !null) then c_idx_ini = idx_ini else if (c_idx_ini ne !null) then idx_ini = c_idx_ini
if (idx_min ne !null) then c_idx_min = idx_min else if (c_idx_min ne !null) then idx_min = c_idx_min
if (path ne !null) then c_path = path
if (xrange eq !null) then xrange = [-1d,1d]
if (yrange eq !null) then yrange = [-1d,1d]

if (~keyword_set(overplot)) then begin
    loadct, 39
    cgdisplay, xsize=800, ysize=800
    cgplot, xarr[0l,*], xarr[1l,*], /nodata, xs=1, ys=1, xr=xrange, yr=yrange, pos=[0.05,0.05,0.95,0.95], iso=(array_equal(xrange,yrange))
    if (keyword_set(cont) and keyword_set(xcont) and keyword_set(ycont)) then begin
        loadct, 34
        cgcontour, (keyword_set(logcolor))? alog(cont) : cont, xcont, ycont, /fill, nlevels=255, /overplot
        loadct, 39
    endif
endif

cgplot, xarr[0,*], xarr[1,*], ps=16, symsize=3, /overplot

n = n_elements(xarr[0,*])

if (carr ne !null) then begin
    for i=0l, n-1l do begin
        for j=0l, carr[i+1]-carr[i]-1l do begin
            cgplot, [xarr[0l,i],xarr[0l,carr[carr[i]+j]]], [xarr[1l,i],xarr[1l,carr[carr[i]+j]]], /overplot
        endfor
    endfor
endif

if (idx_ngb ne !null) then begin
    n_idx_ngb = n_elements(idx_ngb)
    for i=0l, n_idx_ngb-1l do begin
        for j=i+1l, n_idx_ngb-1l do begin
            tmp = where(carr[carr[idx_ngb[i]]:carr[idx_ngb[i]+1l]-1l] eq idx_ngb[j], n_tmp)
            if (n_tmp gt 0l) then begin
                cgplot, [xarr[0l,idx_ngb[i]],xarr[0l,idx_ngb[j]]], [xarr[1l,idx_ngb[i]],xarr[1l,idx_ngb[j]]], thick=3, /overplot
                cgplot, [xarr[0l,idx_ngb[i]],xarr[0l,idx_ngb[j]]], [xarr[1l,idx_ngb[i]],xarr[1l,idx_ngb[j]]], col=192, /overplot
            endif
        endfor
    endfor
    for i=0l, n_idx_ngb-1l do cgplot, [xarr[0l,idx_ngb[i]]], [xarr[1l,idx_ngb[i]]], ps=16, symsize=1, col=192, /overplot
endif

if (idx_src ne !null) then begin
    n_idx_src = n_elements(idx_src)
    for i=0l, n_idx_src-1l do begin
        for j=i+1l, n_idx_src-1l do begin
            tmp = where(carr[carr[idx_src[i]]:carr[idx_src[i]+1l]-1l] eq idx_src[j], n_tmp)
            if (n_tmp gt 0l) then begin
                cgplot, [xarr[0l,idx_src[i]],xarr[0l,idx_src[j]]], [xarr[1l,idx_src[i]],xarr[1l,idx_src[j]]], thick=3, /overplot
                cgplot, [xarr[0l,idx_src[i]],xarr[0l,idx_src[j]]], [xarr[1l,idx_src[i]],xarr[1l,idx_src[j]]], col=250, /overplot
            endif
        endfor
    endfor
    for i=0l, n_idx_src-1l do cgplot, [xarr[0l,idx_src[i]]], [xarr[1l,idx_src[i]]], ps=16, symsize=1, col=250, /overplot
endif

if (idx_ini ne !null) then cgplot, [xarr[0l,idx_ini]], [xarr[1l,idx_ini]], ps=16, symsize=2, col=128, /overplot
if (idx_min ne !null) then cgplot, [xarr[0l,idx_min]], [xarr[1l,idx_min]], ps=16, symsize=2, col=80, /overplot
if (position ne !null) then begin
    cgplot, [position[0,*]], [position[1,*]], ps='x', thick=5, symsize=3, /overplot
    cgplot, [position[0,*]], [position[1,*]], ps='x', thick=2, symsize=2, /overplot, col=250
endif

if (keyword_set(wait)) then wait, wait
if (keyword_set(stop)) then stop
if (c_path ne !null) then begin
    p = file_search(c_path+'*/', count=np)
    if (path ne !null) then begin
        ip = strjoin([replicate('0', 5-strlen(strtrim(np+1l,2))), strtrim(np+1l,2)]) + '/'
        spawn, 'mkdir -p ' + c_path + ip
    endif else ip = strjoin([replicate('0', 5-strlen(strtrim(np,2))), strtrim(np,2)]) + '/'
    f = file_search(c_path+ip+'*.png', count=nf)
    in = strjoin([replicate('0', 5-strlen(strtrim(nf+1l,2))), strtrim(nf+1l,2)]) + '.png'
    write_png, c_path+ip+in, tvrd(/true)
endif

return
end
;______________________________________________________________________________
