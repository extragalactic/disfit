;------------------------------------------------------------------------------
;+
; FILENAME: example__disfit__func.pro
;
; EXAMPLES:
;
;   1)  IDL> .run example__disfit__func
;
; NOTE: Compile IDL Astrolib, MPFIT and ../pro/*.pro files
;
; VERSION: 1.0.210500
;-
;______________________________________________________________________________
;------------------------------------------------------------------------------
; INPUT PARAMETERS:

num     = [2d1,2d1]
xrange  = [-1d,1d]
yrange  = [-1d,1d]
rrange  = [0.5d,1d]
uniform = 1
normal  = 0
regular = 1 
radial  = 0

func = 'rosenbrock'
random = 0
coeff = !null
log = 0

idx_ini = (sort(randomu(seed,product(num))))[0]

extra = { $
    scan: 0, $
    conn_depth: 1, $
    quadratic: 1, $
    quick: 1 $
    }
;______________________________________________________________________________
;------------------------------------------------------------------------------
; POINTS:

xarr = disfit__points(num, xrange=xrange, yrange=yrange, rrange=rrange, uniform=uniform, normal=normal, regular=regular, radial=radial)
yarr = disfit__func(xarr, func=func, coeff=coeff, log=log, random=random)
tmp = min(yarr, idx_min)
;______________________________________________________________________________
;------------------------------------------------------------------------------
; BACKGROUND:

ncont = 5*num[0]+1
xcont = dindgen(ncont)/(ncont-1l)*(xrange[1]-xrange[0]) + xrange[0]
ycont = dindgen(ncont)/(ncont-1l)*(yrange[1]-yrange[0]) + yrange[0]
cont = reform(disfit__func([reform(rebin(xcont,ncont,ncont), 1, ncont*ncont), reform(rebin(transpose(ycont),ncont,ncont), 1, ncont*ncont)], func=func, coeff=coeff, log=log), ncont, ncont)
;______________________________________________________________________________
;------------------------------------------------------------------------------
; DISFIT and PLOT:

idx_pnt = disfit(pnt_arr=xarr, func_name='disfit__func', functest=func, coeff=coeff, log=log, $
    idx_init=idx_ini, conn_depth=conn_depth, nrange=nrange, quadratic=quadratic, $
    idx_pnt=idx_pnt, idx_src=idx_src, idx_ngb=idx_ngb, n_idx_pnt=n_idx_pnt, n_idx_src=n_idx_src, n_idx_ngb=n_idx_ngb, n_pos_min=n_pos_min, ncalls=ncalls, $
    pnt_min=pnt_min, pnt_val=pnt_val, pnt_par_val=pnt_par_val, pnt_par_err=pnt_par_err, $
    pos_min=pos_min, pos_val=pos_val, pos_par_val=pos_par_val, pos_par_err=pos_par_err, $
    points=points, values=values, pvalues=pvalues, perrors=perrors, $
    status=status, errmsg=errmsg, debug=debug, _extra=extra)

print, ''
print, '======================================================================'
print, 'DISFIT RESULT:'
print, '----------------------------------------------------------------------'
print, 'GRID SOLUTION:'
print, pnt_min
print, '----------------------------------------------------------------------'
print, 'NON-GRID SOLUTION:'
print, pos_min
print, '______________________________________________________________________'


qhull, xarr, tri, /delaunay, connectivity=carr
disfit__plot, xarr=xarr, yarr=yarr, carr=carr, idx_ngb=idx_ngb, idx_src=idx_src, position=position, idx_ini=idx_ini, idx_min=idx_min, cont=cont, xcont=xcont, ycont=ycont, path=path, /log
for i_pos=0, n_elements(pos_min[0,*])-1l do cgplot, [pos_min[0,i_pos]], [pos_min[1,i_pos]], col=250, ps=7, symsize=1.5, thick=3, /overplot
;______________________________________________________________________________

print, format='(/,"DONE",/)'
end
